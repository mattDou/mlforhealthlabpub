# Input data 

The original input data is indexed by admission id and processed sequentially by admission id (nb : not by patient). It has a `time` field which is scaled to [0, admission_time_length]. This time field is considered as a dimension of the data (without distinctions from other dimensions). 

Algorithm's input data is classic 3 dimensionnal data : $X \in \mathbb R^{nb_samples x max_len x nb_dims} = R^{N x T x d}$. Stock data is already at this format with n = 1226, T=7 and d=6. 

The data is padded before given to the hider but not **imputed**. Thus, we are forced to take incomplete/padded sequences but we can choose to force imputation or treat it with a custom strategy.

# Seeker logics

I don't understand what is behind the concept of reidentified data. It seems that the seeker has the information of what is generated and what is true data, then classifiy as 0 the len(generated_data) = len(train_data) closest from the generated ones and as 1 the remaining len(test_data).  
I think that I got it : We want to find the test data, so we threw away the closest from the generated data making the assumption that close from the generated data should be training data so not what we are aiming at when seeking the test data. At the opposite, the generator should not replicate the training data but generalize to the true data distribution making it hard to distinguish between test and generated data. From the generator point of view, the best model is sampling from the same distribution as the train and test data, so the seeker cannot do better than 0.5 in accuracy when looking for test data.


# TODO

- The modele cannot reasonnably reconstruct missingness wo having it as entry, we should take as input missingness as well : the challenge does not let 

- 

# Results 
## Results on train_mini

### With add_noise
Feature prediction evaluation on IDs: [26 27 48 22 30]
Feature prediction errors (per feature):
Original data:          [48.5989473443765, 3.9009162900491656, 3.3268749531872426, 4.22975097062652, 0.4194334038646331]
New (hider-generated):  [52.27336855473252, 4.782627518597248, 8.861079586418843, 7.465309624274579, 0.5516156418295893]
One-step-ahead prediction errors (per feature):
Original data:          3718.0986765361504
New (hider-generated):  3718.0799487924046
Reidentification score:                 0.5126

### With timegan