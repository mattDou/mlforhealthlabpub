import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import torch
import torch.nn.functional as F
from torch import nn
from torch.utils.data import Dataset
from common.data.data_preprocess import PADDING_FILL

# Different dataset loaders


class AmdbSimpleDataset(Dataset):
    """
    Dataset feeding torch model for measurements (with nans for missing values and -1 for padded elements) 
    Data should be 2D of shape [n_measurements, n_dimensions]. We do not have any sequence here because 
    we have concatenate all sequences.
    Missing imputation strategy :
     - all nan elements by 0
     (- all padded elements by 0) There should not be any padded elements, 
     but keeping this for better reversibility if we want to keep padded rows in vae 
    """

    def __init__(self, data):
        super(AmdbSimpleDataset, self).__init__()
        self.data = data

    def __getitem__(self, index):
        curr_data = self.data[index]
        # simple fill strategy for missing data (mask)
        missing_mask = np.isnan(curr_data)
        padding_mask = (curr_data == PADDING_FILL)
        curr_data = np.where(missing_mask, 0, curr_data)
        curr_data = np.where(padding_mask, 0, curr_data)
        return torch.Tensor(curr_data).float(), torch.Tensor(missing_mask).float(), torch.Tensor(padding_mask).float()

    def __len__(self):
        return len(self.data)


class AmdbTabularSequenceDataset(Dataset):
    """
    Dataset for sequences of the amdb data.
    Missing imputation strategy :
     - entirely missing column by -1 (might be more interssant to imput by batch mean in a second time)
     - missing values by forward/backward imputation
    """

    def __init__(self, data, max_seq_len):
        super(AmdbTabularSequenceDataset, self).__init__()
        self.data = data
        self.uniq_id = np.unique(data["admissionid"])
        self.no = len(self.uniq_id)
        self.dim = len(data.columns) - 1
        self.median_vals = data.median()
        self.max_seq_len = max_seq_len

    def __getitem__(self, index):
        idx = self.data.index[self.data["admissionid"] == self.uniq_id[index]]
        curr_data = self.data.iloc[idx]
        # Preprocess time
        curr_data["time"] = curr_data["time"] - np.min(curr_data["time"])
        curr_no = len(curr_data)
        preprocessed_data = pd.DataFrame(
            np.ones((self.max_seq_len, curr_data.shape[1] - 1)) * np.nan,
            columns=curr_data.columns[1:],
        )
        if curr_no >= self.max_seq_len:
            preprocessed_data.iloc[:, :] = curr_data.reset_index().iloc[
                : self.max_seq_len, 1:
            ]
        else:
            # I changed to right padding, but to be discussed
            preprocessed_data.iloc[:curr_no,
                                   :] = curr_data.reset_index().iloc[:, 1:]
        missing_cols_index = (
            preprocessed_data.isna().sum(axis=0) == self.max_seq_len
        ).values
        preprocessed_data.loc[:, missing_cols_index] = -1
        missing_values = torch.Tensor(
            preprocessed_data.isna().to_numpy()).float()
        missing_col_values = torch.Tensor(
            (preprocessed_data == -1).to_numpy()).float()
        preprocessed_data = preprocessed_data.bfill()
        preprocessed_data = preprocessed_data.ffill()
        preprocessed_data = torch.Tensor(preprocessed_data.to_numpy()).float()
        return preprocessed_data, missing_values, missing_col_values

    def __len__(self):
        return len(self.uniq_id)


class AmdbSequenceDataset(Dataset):
    """Generate a sequence of tuples

    Args:
        Dataset ([type]): [description]
    """

    def __init__(self, data, max_seq_len, label2ix):
        super(AmdbSequenceDataset, self).__init__()
        self.data = data
        self.uniq_id = np.unique(data["admissionid"])
        self.no = len(self.uniq_id)
        self.dim = len(data.columns) - 1
        self.median_vals = data.median()
        self.max_seq_len = max_seq_len
        self.label2ix = label2ix

    def __getitem__(self, index):
        idx = self.data.index[self.data["admissionid"] == self.uniq_id[index]]
        curr_data = (
            self.data.iloc[idx].drop(
                "admissionid", axis=1).reset_index(drop=True)
        )
        # Preprocess time
        curr_data["time"] = curr_data["time"] - np.min(curr_data["time"])
        columns = curr_data.columns.values
        # print(columns)
        # n_not_na = curr_data.apply(lambda x: (~x[columns].isna()).sum(), axis=1)
        values = np.concatenate([x[~np.isnan(x)]
                                 for x in curr_data.to_numpy()])
        tokens = np.concatenate([columns[~np.isnan(x)]
                                 for x in curr_data.to_numpy()])
        tokens = np.array([self.label2ix[label]
                           for label in tokens], dtype=np.float32)
        tokens_len = len(tokens)
        values_len = len(values)
        assert tokens_len == values_len, "Shapes do not match"

        X = np.array([tokens, values]).transpose()
        if tokens_len >= self.max_seq_len:
            X = X[:self.max_seq_len]
        else:
            # I changed to right padding, but to be discussed
            X = np.concatenate(
                (
                    X,
                    np.repeat(
                        [[self.label2ix["<pad>"], -1]],
                        self.max_seq_len - tokens_len,
                        axis=0,
                    ),
                )
            )
        return torch.Tensor(X).float()

    def __len__(self):
        return len(self.uniq_id)

# custom preprocessing functions


def preproc_vae(data):
    """
    Compute timedelta for the time axis and concatenate all measures wo regards to individual stays
    """
    data_ = np.zeros_like(data)
    for ix in np.arange(data.shape[0]):
        curr_data = data[ix]
        # Create time delta from time column (letting first timestamp at 0)
        curr_data[1:, 0] = curr_data[1:, 0] - curr_data[:-1, 0]
        data_[ix] = curr_data
    # reshape to 2D
    data_ = data_.reshape(-1, data_.shape[2])
    # normalize timedelta
    # Throw away padded elements (always entire rows unnecessary for the vae task)
    padding_mask = (data_ == PADDING_FILL)
    data_ = data_[padding_mask.sum(axis=1) == 0]
    timedelta_mean, timedelta_std = np.mean(data_[:, 0]), np.std(data_[:, 0])
    data_[:, 0] = (data_[:, 0] - timedelta_mean) / timedelta_std
    return(data_)


# Other utils

def cosine_mse_loss(ytrue, ypred, wcs=0.5, wmse=0.5):
    """
    Combines -cosine similarity and mean squared error
    wcs: weight of the cosine similarity
    wmse: weight of the MSE
    """
    return -wcs * F.cosine_similarity(ytrue, ypred).mean() + wmse * nn.MSELoss()(ytrue, ypred)


def plot_learning_history(history1, history2, labels=['train loss', 'validation loss']):
    fig, ax = plt.subplots(1)
    ax.plot(history1, label=labels[0], c='b')
    ax.plot(history2, label=labels[1], c='orange')
    ax.set_title('Evolution of losses')
    ax.legend()
    plt.show()
