import numpy as np
import torch
import torch.nn.functional as F
from torch import nn
from torch.autograd import Variable
from torch.utils.data import DataLoader

from hider.aymericModels.torch_tools import plot_learning_history, ones_target, zeros_target


class Generator(nn.Module):
    """
    generates samples of length maxlen (which is given)
    """

    def __init__(self, maxlen, embedding_dim, noise_dim, hidden_dim):
        super().__init__()
        # dimensions
        self.maxlen = maxlen
        self.noise_dim = noise_dim
        self.hidden_dim = hidden_dim
        self.embedding_dim = embedding_dim
        # layers
        self.lstm = nn.LSTM(noise_dim, hidden_dim, num_layers=1, batch_first=True)
        self.fc = nn.Linear(hidden_dim, embedding_dim)
        # activation
        self.sigmoid = nn.Sigmoid()

    def init_hidden(self, size):
        device = 'cuda' if torch.cuda.is_available() else 'cpu'
        weight = next(self.parameters()).data
        hidden = (weight.new(1, size, self.hidden_dim).zero_().to(device),
                  weight.new(1, size, self.hidden_dim).zero_().to(device))
        return hidden

    def forward(self, x, hidden):
        x, hidden = self.lstm(x, hidden)
        x = self.fc(x)
        x = self.sigmoid(x)
        return x

    def sample(self, n):
        random_noise = generate_noise(n, self.maxlen, self.noise_dim)
        hidden = self.init_hidden(n)
        device = 'cuda' if next(self.parameters()).is_cuda else 'cpu'
        return self.forward(random_noise.to(device), hidden)


class Discriminator(nn.Module):

    def __init__(self, embedding_dim, hidden_dim, prediction_layer='max_pooling'):
        super().__init__()
        # dimensions
        self.embedding_dim = embedding_dim
        self.hidden_dim = hidden_dim
        # layers
        self.lstm = nn.LSTM(embedding_dim, hidden_dim, num_layers=1, batch_first=True)
        self.fc = nn.Linear(hidden_dim, 1)
        self.prediction_layer = prediction_layer

    def init_hidden(self, size):
        device = 'cuda' if torch.cuda.is_available() else 'cpu'
        weight = next(self.parameters()).data
        hidden = (weight.new(1, size, self.hidden_dim).zero_().to(device),
                  weight.new(1, size, self.hidden_dim).zero_().to(device))
        return hidden

    def forward(self, x, hidden):
        x, hidden = self.lstm(x, hidden)
        if self.prediction_layer == 'max_pooling':
            x = x.max(dim=1)[0]
        x = self.fc(x)
        x = F.sigmoid(x)
        return x


class Supervisor(nn.Module):
    """
    generate next sequence using previous sequence 
    """

    def __init__(self, embedding_dim, hidden_dim):
        super().__init__()
        # dimensions
        self.embedding_dim = embedding_dim
        self.hidden_dim = hidden_dim
        # layers
        self.lstm = nn.LSTM(embedding_dim, hidden_dim, num_layers=1, batch_first=True)
        self.fc = nn.Linear(hidden_dim, embedding_dim)
        # activation
        self.sigmoid = nn.Sigmoid()

    def init_hidden(self, size):
        device = 'cuda' if torch.cuda.is_available() else 'cpu'
        weight = next(self.parameters()).data
        hidden = (weight.new(1, size, self.hidden_dim).zero_().to(device),
                  weight.new(1, size, self.hidden_dim).zero_().to(device))
        return hidden

    def forward(self, x, hidden):
        x, hidden = self.lstm(x, hidden)
        x = self.fc(x)
        x = self.sigmoid(x)
        return x


def generate_noise(size, n_steps, dim):
    noise_dim = (size, n_steps, dim)
    noise = Variable(torch.randn(noise_dim)).squeeze()
    return noise


class TimeGAN():

    def __init__(self, autoencoder, maxlen, embedding_dim, noise_dim, hidden_dim, start_idx=2,
                 stop_idx=3, pad_idx=0):
        """
        parameters
        ----------
        autoencoder: pretrained model
        maxlen: int, maximum length observed
        embedding_dim: dimension of the embedding space
        noise_dim: dimension of the noise space that the generator uses
        hidden_dim: dimension of hidden LSTM layers in Discriminator, generator and supervisor  
        """
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        # tokens
        self.start_idx = start_idx
        self.stop_idx = stop_idx
        self.pad_idx = pad_idx
        # dimensions
        self.maxlen = maxlen
        self.embedding_dim = autoencoder.encoding_dim
        self.noise_dim = noise_dim
        self.hidden_dim = hidden_dim
        # networks
        self.generator = Generator(maxlen, embedding_dim, noise_dim, hidden_dim).to(self.device)
        self.discriminator = Discriminator(embedding_dim, hidden_dim).to(self.device)
        self.supervisor = Supervisor(embedding_dim, hidden_dim).to(self.device)
        self.autoencoder = autoencoder.to(self.device)
        # for saving and loading purposes
        self.args = {k: v for k, v in locals().items() if
                     not k in ['self', 'autoencoder', '__class__']}

    def sample(self, n_samples, lens):
        """
        generate random samples
        lens is the list of lens of sequences to generate
        """
        samples = []
        with torch.no_grad():
            for i in range(100):
                hidden = self.autoencoder.init_hidden(self.maxlen)
                encoded_sample = self.generator.sample(n_samples // 100)
                decoded_sample = self.autoencoder.decode(encoded_sample, hidden)
                samples.append(decoded_sample.argmax(dim=2).detach().cpu().numpy())
                if self.device == 'cuda':
                    torch.cuda.empty_cache()
        samples = np.concatenate(samples)
        # set length and deal with tokens
        corrected_samples = np.zeros((n_samples, self.maxlen), dtype=int)
        for i in range(len(samples)):
            sam = samples[i][:lens[i] - 2]
            sam = sam[sam != self.stop_idx]
            sam = sam[sam != self.start_idx]
            sam = sam[sam != self.pad_idx]
            sam = np.concatenate(([self.start_idx], sam, [self.stop_idx],
                                  (self.maxlen - len(sam) - 2) * [self.pad_idx]))
            corrected_samples[i] = sam

        return corrected_samples

    def train_model(self, train_loader, d_optimizer, g_optimizer, s_optimizer, ae_optimizer,
                    n_epochs, print_every_n_epochs, display=False):
        """
        1- train the supervisor 
        2- jointly train supervisor and GAN 
        """

        # define losses
        D_loss = nn.BCELoss()
        S_loss = nn.MSELoss()

        # store training histories
        g_loss_history = np.zeros(n_epochs)
        d_loss_history = np.zeros(n_epochs)
        s_loss_history = np.zeros(n_epochs)

        # activate training mode
        self.generator.train()
        self.discriminator.train()
        self.supervisor.train()
        self.autoencoder.train()

        # train with supervised loss#
        print('Supervised loss training begins')
        for epoch in range(n_epochs):
            s_loss_value = 0
            for n_batch, real_batch in enumerate(train_loader):
                s_optimizer.zero_grad()
                ae_optimizer.zero_grad()
                n = real_batch.size()[0]
                X = real_batch.to(self.device)
                # initalize hidden states
                hidden_ae = self.autoencoder.init_hidden(n)
                hidden_s = self.supervisor.init_hidden(n)
                # encode and predict next
                H = self.autoencoder.encode(X, hidden_ae)
                H_supervised = self.supervisor(H, hidden_s)
                # compute loss and back prop
                loss = S_loss(H_supervised[:, :-1, :], H[:, 1:, :].detach())
                loss.backward()
                s_optimizer.step()
                ae_optimizer.step()
                s_loss_value += loss.detach().cpu().numpy()

                del X, real_batch, hidden_ae, hidden_s, H, H_supervised, loss
                torch.cuda.empty_cache()

            if epoch % print_every_n_epochs == 0:
                print(f'At epoch {epoch}: supervisor loss {s_loss_value / n_batch:.5f}')

        # joint training#
        print('Joint training begins')
        for epoch in range(n_epochs):

            generator_loss = 0
            discriminator_loss = 0
            for n_batch, real_batch in enumerate(train_loader):
                d_optimizer.zero_grad()
                g_optimizer.zero_grad()
                s_optimizer.zero_grad()
                ae_optimizer.zero_grad()
                X = real_batch.to(self.device)
                N = X.size()[0]
                ###generator training
                Z = generate_noise(N, self.maxlen, self.noise_dim).to(self.device)
                # init hidden states
                hidden_g = self.generator.init_hidden(N)
                hidden_s = self.supervisor.init_hidden(N)
                hidden_d = self.discriminator.init_hidden(N)
                hidden_ae = self.autoencoder.init_hidden(N)
                # generate, supervise, discriminate
                E_hat = self.generator(Z, hidden_g)
                H_hat = self.supervisor(E_hat, hidden_s)
                Y_fake = self.discriminator(H_hat, hidden_d)
                # compute loss and backprop
                loss = D_loss(Y_fake, ones_target(N).to(self.device)) + S_loss(H_hat[:, :-1, :],
                                                                               E_hat[:, 1:,
                                                                               :].detach())
                loss.backward()
                g_optimizer.step()
                s_optimizer.step()
                g_loss_ = loss.detach().cpu().numpy()
                generator_loss += g_loss_

                ###discriminator training
                d_optimizer.zero_grad()
                g_optimizer.zero_grad()
                s_optimizer.zero_grad()
                ae_optimizer.zero_grad()
                # re-init hidden states
                hidden_g = self.generator.init_hidden(N)
                hidden_s = self.supervisor.init_hidden(N)
                hidden_d = self.discriminator.init_hidden(N)
                hidden_ae = self.autoencoder.init_hidden(N)
                # generate new data
                Z = generate_noise(N, self.maxlen, self.noise_dim).to(self.device)
                E_hat = self.generator(Z, hidden_g)
                H_hat = self.supervisor(E_hat, hidden_s)
                H = self.autoencoder.encode(X, hidden_ae)
                # compute predictions
                Y_real = self.discriminator(H, hidden_d)
                Y_fake = self.discriminator(H_hat, hidden_d)
                Y_fake_e = self.discriminator(E_hat, hidden_d)
                # compute losses
                loss = D_loss(Y_real, ones_target(N).to(self.device)) + D_loss(Y_fake,
                                                                               zeros_target(N).to(
                                                                                   self.device)) + D_loss(
                    Y_fake_e, zeros_target(N).to(self.device))
                # train discriminator only if loss is high enough
                if loss.item() > g_loss_:
                    loss.backward()
                    d_optimizer.step()
                    ae_optimizer.step()
                discriminator_loss += loss.detach().cpu().numpy()

                del real_batch, X, Z, hidden_g, hidden_s, hidden_d, hidden_ae, E_hat, H_hat, Y_fake, loss, H, Y_real
                torch.cuda.empty_cache()

            if epoch % print_every_n_epochs == 0:
                print(
                    f'At epoch {epoch}: discriminator loss {discriminator_loss / n_batch}, generator loss {generator_loss / n_batch} ')
            g_loss_history[epoch] = generator_loss / n_batch
            d_loss_history[epoch] = discriminator_loss / n_batch

        if display:
            plot_learning_history(g_loss_history, d_loss_history, ['generator', 'discriminator'])
        self.generator.eval(), self.discriminator.eval(), self.supervisor.eval()

        return g_loss_history, d_loss_history

    def evaluate_model(self, n_samples, validation_set):
        loss = nn.BCELoss()

        with torch.no_grad():
            # compute accuracy on fake data, batch wise
            predictions = []
            for i in range(100):
                hidden_d = self.discriminator.init_hidden(n_samples // 100)
                samples = self.sample(n_samples // 100)
                pred = self.discriminator(samples.to(self.device), hidden_d)
                pred = pred.round().detach().cpu().numpy()
                predictions.append(pred)
            predictions = np.concatenate(predictions)
            accuracy_fake = np.mean(predictions == 0)
            # compute accuracy on real data
            predictions = []
            hidden_d = self.discriminator.init_hidden(n_samples // 100)
            hidden_ae = self.autoencoder.init_hidden(n_samples // 100)
            loader = DataLoader(validation_set, shuffle=True, batch_size=n_samples // 100)
            predictions = []
            for i, samples in enumerate(loader):
                if i > 100:
                    break
                encoded = self.autoencoder.encode(samples.to(self.device), hidden_ae)
                predictions.append(
                    self.discriminator(encoded, hidden_d).round().detach().cpu().numpy())
            predictions = np.concatenate(predictions)
            accuracy_real = np.mean(predictions == 1)
        acc = {'accuracy_real': accuracy_real, 'accuracy_fake': accuracy_fake}
        return acc
