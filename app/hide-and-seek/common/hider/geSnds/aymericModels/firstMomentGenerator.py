import numpy as np 

class FirstMomentGenerator():
    """
    Baseline estimator for 1-dimensional binary data. 
    Each component is assumed to follow a Bernoulli distribution independantly from the others. 
    """
    def __init__(self):
        self.dist = 'bernoulli'
        self.probs = None
        
    def fit(self,X):
        #convert to binary if not already done
        if np.max(X)!=1 or np.min(X)!=0:
            X = (X>0).astype(float)
        #estimate parameters of the Bernoullis
        self.probs = np.mean(X,axis=0).squeeze()
        
        
    def sample(self,n_samples):
        #generate binomial samples with n=1
        bins = np.random.binomial(1,self.probs,size=(n_samples,self.probs.shape[0]))
        return bins
        