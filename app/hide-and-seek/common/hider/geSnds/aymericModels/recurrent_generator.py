import json

import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn

from hider.aymericModels.torch_tools import plot_learning_history


def gumbel_sample(size):
    uniforms = torch.rand(size)
    gumbel = -torch.log(-torch.log(uniforms))
    return gumbel


class Rnn(nn.Module):
    def __init__(
            self,
            input_dim,
            maxlen=100,
            n_layers=1,
            hidden_dim=128,
            dropout=0.5,
            dataloader_nbworkers=8,
            verbose: int = 1):

        super().__init__()

        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.input_dim = input_dim
        self.maxlen = maxlen
        self.hidden_dim = hidden_dim
        self.n_layers = n_layers
        self.dropout = dropout
        self.dataloader_nbworkers = dataloader_nbworkers

        # dropout layer for regularization
        self.drop = nn.Dropout(self.dropout)
        self.bn1 = nn.BatchNorm1d(self.hidden_dim)

        # recurrent unit layer (lstm because this is robust, but could try with GRU cells)
        self.recurrent = nn.LSTM(
            input_size=self.input_dim,
            hidden_size=self.hidden_dim,
            num_layers=n_layers,
            batch_first=True
        )
        # map the hidden state to the vocabulary size to predict next word
        self.hidden2output = nn.Linear(self.hidden_dim, self.input_dim)
        self.sep_softmax = nn.Softmax(dim=1)

        # for saving and loading purposes
        args = locals()
        self.args = {k: v for k, v in args.items() if not k in ['self', '__class__']}

    def init_hidden(self, batch_size):
        # Before we've done anything, we don't have any hidden state.
        # The axes semantics are (num_layers, minibatch_size, hidden_dims)
        weight = next(self.parameters())
        return (
            weight.new_zeros(self.n_layers, batch_size, self.hidden_dim).to(self.device),
            weight.new_zeros(self.n_layers, batch_size, self.hidden_dim).to(self.device)
        )

    def forward(self, batch, hidden):
        batch_size, seq_len, data_dim = batch.size()
        # pack_padded_sequence to hide padded items to the recurrent layer
        rec_out, hidden = self.recurrent(batch, hidden)
        rec_out = rec_out.contiguous()
        rec_out_resized = rec_out.view(-1, self.hidden_dim)
        out = self.hidden2voca(rec_out_resized)
        # log-softmax
        probs = self.sep_softmax(out)
        # consider using adaptive soft max for imbalance distribution over outputs:
        # https://pytorch.org/docs/stable/nn.html?highlight=logsoftmax#torch.nn.AdaptiveLogSoftmaxWithLoss

        #  (batch_size * seq_len, nb_hidden) -> (batch_size, seq_len, vocabulary_size)
        return probs.view(batch_size, seq_len, self.vocabulary_size), hidden

    def loss(self, y_pred, y):
        """A loss function that deals with padded items and multi-task learning"""

        y = y.contiguous().view(-1)
        # flatten all predictions
        y_pred = y_pred.contiguous().view(-1, self.vocabulary_size)
        # create a mask by filtering out all tokens that ARE NOT the padding token
        tag_pad_token = self.padding_idx
        mask = (y != tag_pad_token).float()
        # count how many tokens we have
        nb_tokens = int(torch.sum(mask).item())

        # weighted Cross-entropy loss with the mask  as weights (ignore <PAD> tokens)
        y_pred = y_pred[range(y_pred.shape[0]), y] * mask
        ce_loss = -torch.sum(y_pred) / nb_tokens

        return ce_loss

    def train_model(
            self,
            train_loader,
            val_loader,
            optimizer,
            scheduler=None,
            nb_epochs=10,
            print_every_n_epochs=1,
            display=False
    ):

        # initialise history
        train_loss_history = []
        validation_loss_history = []
        train_total_loss = 0.
        dev_total_loss = 0.

        self.to(self.device)

        # TODO: let epoch be a parameter of the network
        epoch = 0
        batch_size = train_loader.batch_size
        for epoch in range(nb_epochs):
            train_epoch_loss = 0.
            # setup the model in train mode
            self.train()
            for i, (local_batch, local_labels) in enumerate(
                    train_loader):
                optimizer.zero_grad()
                self.zero_grad()

                x = local_batch.long().to(self.device)
                y = local_labels.long().to(self.device)
                hidden = self.init_hidden(x.size(0))
                y_pred, _ = self.forward(x, hidden)

                loss = self.loss(y_pred, y)
                loss.backward()
                optimizer.step()
                train_epoch_loss += loss.detach().cpu()
                # clear memory
                del x, y, y_pred, loss, _, local_batch, local_labels, hidden
                torch.cuda.empty_cache()

            n_train_batch = i
            dev_epoch_loss = 0.
            # Setup the model in eval mode (does not compute gradients)
            self.eval()
            with torch.no_grad():
                for i, (local_batch, local_labels) in enumerate(
                        val_loader):
                    x = local_batch.long().to(self.device)
                    y = local_labels.long().to(self.device)
                    hidden = self.init_hidden(batch_size)

                    y_pred, _ = self.forward(x, hidden)
                    loss = self.loss(y_pred, y)

                    dev_epoch_loss += loss.detach().cpu()
                    # clear memory
                    del x, y, y_pred, loss, _, local_batch, local_labels, hidden
                    torch.cuda.empty_cache()
                n_val_batch = i

            if not scheduler is None:
                scheduler.step(dev_epoch_loss)

            train_total_loss += train_epoch_loss
            dev_total_loss += dev_epoch_loss
            train_loss_history.append(train_epoch_loss / n_train_batch)
            validation_loss_history.append(dev_epoch_loss / n_val_batch)

            if epoch % print_every_n_epochs == 0:
                print(
                    "\n Epoch {:.0f}, Train epoch Loss = {:.4f}, Dev epoch Loss = {:.4f}".format(
                        epoch,
                        train_epoch_loss / n_train_batch,
                        dev_epoch_loss / n_val_batch
                    )
                )
                # print("Total train Loss: {}".format(train_total_loss))

        print("------------------------------------------------")
        print(
            "\n Training ended, \n Total train Loss = {:.4f}, Total dev Loss = {:.4f}".format(
                train_total_loss, dev_total_loss))
        if display:
            plot_learning_history(train_loss_history, validation_loss_history)

        return train_loss_history, validation_loss_history, epoch

    def evaluate(self,
                 val_loader,
                 k=1,
                 verbose=True):

        # not enough memory on gpu to keep the whole data set as tensors
        eval_model = self.to(self.device)
        eval_model.eval()
        accuracies = []
        with torch.no_grad():
            for i, (batch, labels) in enumerate(
                    val_loader):

                batch = batch.to(self.device)
                labels = labels.to(self.device)
                hidden = eval_model.init_hidden(batch.size()[0])

                batch_y_pred, hidden = eval_model.forward(batch, hidden)
                batch_topks = batch_y_pred.topk(k, dim=2)[1]
                binary_accu = torch.zeros(batch.size()).to(self.device)
                for i in range(k):
                    binary_accu += (batch_topks[:, :, i] == labels).float()
                binary_accu = (binary_accu > 0).float()
                mask = (labels != self.padding_idx).float()
                binary_accu *= mask
                accuracy = binary_accu.sum() / mask.sum()
                accuracies.append(accuracy)

        return np.mean(accuracies)

    def evaluate_every_k(self, val_loader, k_max, save_path=None):
        accuracy_list = np.zeros(k_max)
        for k in range(k_max):
            accuracy_list[k] = self.evaluate(val_loader, k + 1, verbose=False)
        fig, ax = plt.subplots(1, 1)
        ax.plot(np.arange(1, k_max + 1), accuracy_list, marker='o')
        ax.set_title(f'Accuracy in top {k_max}')
        if not save_path is None:
            plt.savefig(save_path)
        plt.show()

        return accuracy_list

    def sample_(self,
                starts,
                lens,
                gumbel_max_trick=True,
                ):

        """
        Mainly an auxiliary function for sample_sequences
        
        parameters
        ----------
        starts: torch Tensor of shape (n_samples,n_steps,vocab_size)
        n_steps_max: int, numbr of steps to sample
        gumbel_max_trick: bool, whether to implement the gumbel max trick while sampling (enhances diversity)
        
        returns
        -------
        padded_starts: torch Tensor of shape (n_samples,n_steps_max,vocab_size)
        """
        with torch.no_grad():
            padded_starts = nn.utils.rnn.pad_sequence(starts, padding_value=self.padding_idx,
                                                      batch_first=True).long().to(self.device)
            hidden = self.init_hidden(len(starts))
            for i in range(self.maxlen - starts.size(1)):
                out, hidden = self.forward(padded_starts, hidden)
                if gumbel_max_trick:
                    gumbel = gumbel_sample(out.size()).to(self.device)
                    out += gumbel
                out = out.argmax(dim=2)
                # add last column to starts
                last = out[:, -1].view(-1, 1)
                last_oh = torch.zeros(last.size()[0], self.vocabulary_size).to(self.device)
                last_oh = last_oh.scatter_(1, last, 1)
                starts = torch.cat((starts, last), dim=1)
                padded_starts = nn.utils.rnn.pad_sequence(starts, padding_value=self.padding_idx,
                                                          batch_first=True)

        sample = padded_starts.detach().cpu().numpy()
        corrected_samples = np.zeros((len(sample), self.maxlen), dtype=int)
        for i in range(len(sample)):
            sam = sample[i][:lens[i] - 2]
            sam = sam[sam != self.stop_idx]
            sam = sam[sam != self.start_idx]
            sam = sam[sam != self.padding_idx]
            sam = np.concatenate(([self.start_idx], sam, [self.stop_idx],
                                  (self.maxlen - len(sam) - 2) * [self.padding_idx]))
            corrected_samples[i] = sam

        return corrected_samples

    def sample_sequences(self, loader, n_samples, lens, starts_len=5, return_real=True):
        """
        main function for sampling sequences
        """
        samples = []
        if return_real:
            real = []
        max_iter = n_samples // loader.batch_size
        with torch.no_grad():
            for i, (batch, labels) in enumerate(loader):
                if i > max_iter:
                    break
                if return_real:
                    real_batch = np.concatenate((self.start_idx * np.ones((batch.size()[0], 1),
                                                                          dtype=int),
                                                 labels.detach().cpu().numpy()), axis=1)
                    real.append(real_batch)
                starts = batch[:, :starts_len].long().to(self.device)
                sample = self.sample_(starts, lens, gumbel_max_trick=True)
                samples.append(sample)
                torch.cuda.empty_cache()
        sample = np.concatenate(samples, axis=0)[:n_samples]
        real = np.concatenate(real, axis=0)[:n_samples]

        return sample, real
