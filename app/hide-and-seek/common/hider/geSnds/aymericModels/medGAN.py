from torch import nn
import torch
import torch.nn.functional as F
from torch.autograd import Variable
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import train_test_split
import numpy as np
import matplotlib.pyplot as plt
from ...utils.torch_tools import plot_learning_history,generate_noise,ones_target,zeros_target
from torch.utils.data import DataLoader
from .FCGAN import FCGAN, Discriminator
from collections import OrderedDict

class MedGAN(FCGAN):
    """
    generates in the latent space, autoencoder decodes 
    the block generator+autoencoder is trained simultaneously
    the discriminator works on data in the original space
    """
    
    def __init__(self,autoencoder,noise_dim,g_hidden_dims,d_hidden_dims,num_words):
        super().__init__(autoencoder,noise_dim,g_hidden_dims,d_hidden_dims)
        #freeze encoding layers of the autoencoder
        self.autoencoder.trainable_params = []
        restrained_state_dict = OrderedDict({k:v for k,v in self.autoencoder.state_dict().items() if not 'running' in k})
        for name,param in zip(restrained_state_dict,self.autoencoder.parameters()):
            if 'encoding' in name:
                param.requires_grad = False
            if 'decoding' in name:
                param.requires_grad = True
                self.autoencoder.trainable_params.append(param)
        #correct discriminator
        self.d_hidden_dims = d_hidden_dims + (1,)
        self.discriminator = Discriminator(num_words,self.d_hidden_dims)
        #for loading and saving purposes
        self.args['num_words'] = num_words
        self.args['d_hidden_dims'] = self.d_hidden_dims
        
    def train_model(self,train_loader,g_optimizer,d_optimizer, dec_optimizer,
                    n_epochs,label_smoothing = 0.9,print_every_n_epoch=1,display=False):
        """
        parameters
        ----------
        train_loader: torch DataLoader
        g_optimizer, d_optimizer,dec_optimizer: optimizers for generator, discriminator and autoencoder (resp.)
        n_epochs: int, number of epochs of training
        label_smoothing: float, 0<x<1. When training discriminator, the targets are label_smoothing*1. instead of 1 for real data
        print_every_n_epoch: int
        display: bool, if True plot learning history otherwise not
        
        NB: 1 indicates real, 0 indicates fake
        """

        self.autoencoder.to(self.device)
        self.generator.to(self.device)
        self.discriminator.to(self.device)
        #define loss 
        loss = nn.BCELoss()

        g_loss_history = np.zeros(n_epochs)
        d_loss_history = np.zeros(n_epochs)
        self.generator.train()
        self.discriminator.train()
        self.autoencoder.train()

        for epoch in range(n_epochs):
            generator_loss = 0
            discriminator_loss = 0
            for n_batch, real_batch in enumerate(train_loader):
                
                real_batch = real_batch.to(self.device)
                N = real_batch.size(0)
                #----------------
                # Train generator
                #----------------
                #generate fake data
                fake_data = self.generator.sample(N)
                fake_data = self.autoencoder.decode(fake_data)
                #compute error
                g_optimizer.zero_grad()
                prediction = self.discriminator(fake_data)
                gdec_error = loss(prediction, ones_target(N).to(self.device))
                gdec_error.backward()
                g_optimizer.step()
                dec_optimizer.step()
                #--------------------
                # Train discriminator
                #--------------------
                # Generate fake data and detach
                fake_data = self.generator.sample(N).detach()
                fake_batch = self.autoencoder.decode(fake_data).detach()
                #zero grad
                d_optimizer.zero_grad()
                #error on real data
                prediction_real = self.discriminator(real_batch)
                error_real = loss(prediction_real, label_smoothing * ones_target(N).to(self.device) )
                #error on fake data
                prediction_fake = self.discriminator(fake_batch)
                error_fake = loss(prediction_fake, zeros_target(N).to(self.device))
                d_error = error_real + error_fake
                d_error.backward()
                d_optimizer.step()
                #update losses
                generator_loss += gdec_error.detach().cpu().numpy()
                discriminator_loss += d_error.detach().cpu().numpy()
                #free memory
                del real_batch,fake_data,prediction,fake_batch,gdec_error,prediction_fake
                if self.device=='cuda':
                    torch.cuda.empty_cache()

            g_loss_history[epoch] = generator_loss/n_batch
            d_loss_history[epoch] = discriminator_loss/n_batch

            if (epoch+1) % print_every_n_epoch == 0:
                print(f'Epoch {epoch} generator loss {generator_loss/n_batch} discriminator loss {discriminator_loss/n_batch}')

        if display:
            plot_learning_history(g_loss_history,d_loss_history,['generator','discriminator'])
        self.generator.eval(),self.discriminator.eval(),self.autoencoder.eval()
        
        return g_loss_history,d_loss_history

    
    def evaluate_model(self,n_samples,validation_set):
        """
        Compute accuracy on real unseen data and synthetic data
        
        parameters
        ----------
        n_samples: int, number of samples to generate, and number of samples to take from validation_set
        validation_set: BOEDataset instance
        
        returns
        -------
        acc: dict with keys 'accuracy_real' and 'accuracy_fake'
        """
        #compute accuracy on fake data
        samples = self.autoencoder.decode(self.generator.sample(n_samples))
        predictions = self.discriminator(samples)
        predictions = predictions.round().detach().cpu().numpy()
        accuracy_fake = np.mean(predictions==0)

        #compute accuracy on real data
        loader = DataLoader(validation_set,shuffle=True,batch_size=n_samples)
        samples = next(iter(loader))
        predictions = self.discriminator(samples.to(self.device)).round().detach().cpu().numpy()
        accuracy_real = np.mean(predictions==1)
        acc = {'accuracy_real':accuracy_real,'accuracy_fake':accuracy_fake}
        return acc



