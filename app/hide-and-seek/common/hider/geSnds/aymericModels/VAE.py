import torch
import torch.utils.data
from torch import nn, optim
from torch.nn import functional as F
from torch_tools import plot_learning_history, cosine_mse_loss
import numpy as np


class VAE(nn.Module):

    def __init__(self, input_dim, hidden_dims):

        super().__init__()
        # encoding layers
        layer_dims = (input_dim,) + hidden_dims
        n = len(layer_dims)
        self.hidden_encoding = nn.ModuleList()
        for k in range(len(layer_dims)-2):
            self.hidden_encoding.append(
                nn.Linear(layer_dims[k], layer_dims[k+1]))
            self.hidden_encoding.append(nn.Dropout(p=0.2))
        self.encoding_layer = nn.Linear(layer_dims[-2], layer_dims[-1])
        self.distribution_layer = nn.Linear(layer_dims[-2], layer_dims[-1])

        # decoding layers
        self.hidden_decoding = nn.ModuleList()
        for k in reversed(range(1, len(layer_dims))):
            self.hidden_decoding.append(
                nn.Linear(layer_dims[k], layer_dims[k-1]))
            self.hidden_decoding.append(nn.Dropout(p=0.2))

    def encode(self, x):
        for transfo in self.hidden_encoding:
            x = F.relu(transfo(x))
        return self.encoding_layer(x), self.distribution_layer(x)

    def reparameterize(self, mu, logvar):
        std = torch.exp(0.5*logvar)
        eps = torch.randn_like(std)
        return mu + eps*std

    def decode(self, z):
        for transfo in self.hidden_decoding:
            z = F.relu(transfo(z))
        return z

    def forward(self, x):
        mu, logvar = self.encode(x.squeeze())
        z = self.reparameterize(mu, logvar)
        return self.decode(z), mu, logvar


# Reconstruction + KL divergence losses summed over all elements and batch
def loss_function(recon_x, x, mu, logvar):
    BCE = cosine_mse_loss(recon_x, x.squeeze())

    # see Appendix B from VAE paper:
    # Kingma and Welling. Auto-Encoding Variational Bayes. ICLR, 2014
    # https://arxiv.org/abs/1312.6114
    # 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
    KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())

    return BCE + KLD


def trainVAE(model, train_loader, val_loader, optimizer, n_epochs, print_every_n_epoch, display=False):
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    model = model.to(device)
    train_loss_history = np.zeros(n_epochs)
    val_loss_history = np.zeros(n_epochs)

    for epoch in range(n_epochs):

        model.train()
        train_loss = 0
        for batch_idx, data in enumerate(train_loader):
            data = data.to(device)
            optimizer.zero_grad()
            recon_batch, mu, logvar = model(data)
            loss = loss_function(recon_batch, data, mu, logvar)
            loss.backward()
            train_loss += loss.item()
            optimizer.step()
        n_train_batches = batch_idx
        train_loss_history[epoch] = train_loss/n_train_batches
        # validation step
        model.eval()
        val_loss = 0
        for batch_idx, data in enumerate(val_loader):
            data = data.to(device)
            recon_batch, mu, logvar = model(data)
            loss = loss_function(recon_batch, data, mu, logvar)
            val_loss += loss.item()
        n_val_batches = batch_idx
        val_loss_history[epoch] = val_loss/n_val_batches

        if epoch % print_every_n_epoch == 0:
            print('Epoch: {} Average training loss: {:.4f}, average validation loss: {:.4f}'.format(
                  epoch, train_loss / len(train_loader.dataset), val_loss / len(val_loader.dataset)))

    if display:
        plot_learning_history(train_loss_history, val_loss_history)
    return train_loss_history, val_loss_history
