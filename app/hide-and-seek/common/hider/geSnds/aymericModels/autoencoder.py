import numpy as np
import torch
from scipy import sparse
from torch import nn

from ...utils.early_stopping import EarlyStopping
from ...utils.evaluation_sboe import evaluate_reconstruction
from ...utils.torch_tools import plot_learning_history, cosine_mse_loss


class AE(nn.Module):
    def __init__(self, input_dim, hidden_dims, binary=True):
        """
        builds symmetrical autoencoder
        :input_dim: int
        :hidden_dims: iterable of ints
        :binary: bool, whether to add a sigmoid layer at the end
        """
        super().__init__()
        self.is_binary = binary
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        # for saving and loading purposes
        args = locals()
        self.args = {k: v for k, v in args.items() if not k in ['self', '__class__']}
        # encoding layers
        layer_dims = (input_dim,) + hidden_dims
        self.encoding = nn.ModuleList()
        self.encoding_bn = nn.ModuleList()
        self.encoding_relu = nn.ModuleList()
        self.encoding_dropout = nn.ModuleList()

        for k in range(len(layer_dims) - 1):
            self.encoding.append(nn.Linear(layer_dims[k], layer_dims[k + 1]))
            self.encoding_bn.append(nn.BatchNorm1d(layer_dims[k + 1]))
            self.encoding_relu.append(nn.ReLU(inplace=True))
            self.encoding_dropout.append(nn.Dropout(p=0.2))

        # decoding layers
        self.decoding = nn.ModuleList()
        self.decoding_bn = nn.ModuleList()
        self.decoding_relu = nn.ModuleList()
        self.decoding_dropout = nn.ModuleList()

        for k in reversed(range(2, len(layer_dims))):
            self.decoding.append(nn.Linear(layer_dims[k], layer_dims[k - 1]))
            self.decoding_bn.append(nn.BatchNorm1d(layer_dims[k - 1]))
            self.decoding_relu.append(nn.ReLU(inplace=True))
            self.decoding_dropout.append(nn.Dropout(p=0.2))
        self.decoding_layer = nn.Linear(layer_dims[1], layer_dims[0])

        if binary:
            self.binary_layer = nn.Sigmoid()
        else:
            self.relu_layer = nn.ReLU(inplace=True)

    def forward(self, x):
        x = self.encode(x)
        x = self.decode(x)

        return x

    def encode(self, x):
        for lin, bn, relu, do in zip(self.encoding, self.encoding_bn, self.encoding_relu,
                                     self.encoding_dropout):
            x = lin(x)
            x = bn(x.squeeze(1))
            x = relu(x)
            x = do(x)
            # x = do(relu(bn(lin(x).squeeze(1))))

        return x

    def decode(self, x):
        for lin, bn, relu, do in zip(self.decoding, self.decoding_bn, self.decoding_relu,
                                     self.decoding_dropout):
            x = lin(x)
            x = bn(x)
            x = relu(x)
            x = do(x)
            # x = do(relu(bn(lin(x))))
        x = self.decoding_layer(x)
        if self.is_binary:
            x = self.binary_layer(x)
        else:
            x = self.relu_layer(x)
        return x

    def train_model(self, train_loader, val_loader, optimizer, n_epochs,
                    activate_early_stopping=False, print_every_n_epoch=1, display=False):

        """
        :param (int) activate_early_stopping: if 0, no early stopping. Otherwise, early stopping with the param as patience
        """
        self.to(self.device)
        train_loss_history = []
        val_loss_history = []
        # select criterion according to the objective
        if self.is_binary:
            criterion = nn.BCELoss(reduce=True, size_average=True)
        else:
            criterion = cosine_mse_loss
        # train
        self.train()
        # early stopping
        if activate_early_stopping:
            early_stopping = EarlyStopping(patience=activate_early_stopping, verbose=True)

        for epoch in range(n_epochs):
            # ---------
            # Training
            # ---------
            train_loss = 0
            for it, data in enumerate(train_loader):
                data = data.to(self.device)
                optimizer.zero_grad()
                output = self.forward(data)
                if self.is_binary:
                    data = (data > 0).float()

                loss = criterion(output, data.squeeze())
                loss.backward()
                optimizer.step()
                train_loss += loss.detach().cpu().numpy()

            n_train_batches = it
            train_loss_history.append(train_loss / n_train_batches)
            # -----------
            # Evaluation
            # -----------
            val_loss = 0
            self.eval()
            for it, data in enumerate(val_loader):
                data = data.to(self.device)
                output = self.forward(data)
                loss = criterion(output, data.squeeze()).detach().cpu().numpy()
                val_loss += loss
            n_val_batches = it
            val_loss_history.append(val_loss / n_val_batches)
            if (epoch + 1) % print_every_n_epoch == 0:
                print(
                    f'Epoch {epoch} training loss {train_loss / n_train_batches} validation loss {val_loss / it}')
            # ---------------
            # Early stopping
            # ---------------
            if activate_early_stopping:
                early_stopping(val_loss)
                if early_stopping.early_stop:
                    break
            self.train()

        if display:
            plot_learning_history(train_loss_history, val_loss_history)
        self.eval()
        return train_loss_history, val_loss_history

    def evaluate_model(self, val_loader):
        """
        Reconstructs the data of the val_loader
        Computes the proportion of non-null vectors, the jaccard index, the support portion detected, 
        the classification scores using a logit or a random forest on the reconstructed data. 
        
        :param (torch.data.utils.DataLoader) val_loader: loader which contains the validation data
        :returns (dict) evaluation: dict with keys 'non_null_prop','jaccard','support_recall','accuracy_rf','recall_rf','accuracy_logit','recall_logit'
        """
        # gather predictions and ground truth
        predictions = []
        truths = []
        for truth in val_loader:
            truth = truth.to(self.device)
            autoencoded = self.forward(truth).round().detach().cpu().numpy()
            truth = truth.squeeze().detach().cpu().numpy()
            predictions.append(autoencoded)
            truths.append(truth)

        # compute scores
        truths = np.concatenate(truths)
        predictions = np.concatenate(predictions)
        evaluation = evaluate_reconstruction(truths, predictions)
        return evaluation

    def reconstruct_and_save(self, val_loader, save_dir=None):
        """
        Forward pass on a whole loader, saves the results as a .npy file in compressed format
        
        Parameters
        ----------
        val_loader: torch DataLoader
        save_dir: None or str, where to save the outputs. If None, no saving of the results
        
        Returns
        -------
        truth: np.array, val_loader converted to numpy
        samples: np.array, reconstructed val_loader
        """
        samples_list = []
        truth_list = []
        for batch in val_loader:
            batch = (batch > 0).float()
            autoencoded = self.forward(batch.to(self.device))
            sample = autoencoded.round().detach().cpu().numpy()
            batch = batch.squeeze().cpu().numpy()
            samples_list.append(sample)
            truth_list.append(batch)

        truth = np.concatenate(truth_list)
        samples = np.concatenate(samples_list)
        if not save_dir is None:
            np.save(save_dir, (sparse.csr_matrix(truth), sparse.csr_matrix(samples)))
        return truth, samples
