import numpy as np
import torch
import torch.nn.functional as F
from torch import nn

from hider.aymericModels.torch_tools import plot_learning_history


class RecurrentAE(nn.Module):

    def __init__(self, vocab_size, embedding_dim, hidden_dim, encoding_dim, padding_idx, stop_idx):
        """
        builds symmetrical autoencoder 
        :input_dim: int
        :hidden_dims: iterable of ints
        :binary: bool, whether to add a sigmoid layer at the end 
        """
        super().__init__()
        self.padding_idx = padding_idx
        self.stop_idx = stop_idx
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        # dimensions
        self.vocab_size = vocab_size
        self.embedding_dim = embedding_dim
        self.hidden_dim = hidden_dim
        self.encoding_dim = encoding_dim
        # encoding layers
        self.embedding = nn.Embedding(vocab_size, embedding_dim, padding_idx=padding_idx)
        self.encoding_lstm = nn.LSTM(embedding_dim, hidden_dim, num_layers=1, batch_first=True)
        self.encoding_fc = nn.Linear(hidden_dim, encoding_dim)
        # decoding layers
        self.dec_lstm = nn.LSTM(encoding_dim, hidden_dim, num_layers=1)
        self.out_layer = nn.Linear(hidden_dim, vocab_size)
        self.logsoftmax = nn.LogSoftmax(dim=1)
        # for saving and loading purposes
        args = locals()
        self.args = {k: v for k, v in args.items() if not k in ['self', '__class__']}

    def forward(self, x, hidden_enc, hidden_dec):

        x = self.encode(x, hidden_enc)
        x = self.decode(x, hidden_dec)

        return x

    def init_hidden(self, size):
        weight = next(self.parameters()).data
        hidden = (weight.new(1, size, self.hidden_dim).zero_().to(self.device),
                  weight.new(1, size, self.hidden_dim).zero_().to(self.device))
        return hidden

    def encode(self, x, hidden):
        x = self.embedding(x)
        x, hidden = self.encoding_lstm(x, hidden)
        x = self.encoding_fc(x)
        x = F.sigmoid(x)
        return x

    def decode(self, x, hidden):
        x, hidden = self.dec_lstm(x, hidden)
        x = self.logsoftmax(self.out_layer(x))
        return x

    def loss(self, y_pred, y):
        """A loss function that deals with padded items and multi-task learning"""

        y = y.contiguous().view(-1)
        # flatten all predictions
        y_pred = y_pred.contiguous().view(-1, self.vocab_size)
        # create a mask by filtering out all tokens that ARE NOT the padding token
        tag_pad_token = self.padding_idx
        mask = (y != tag_pad_token).float()
        # count how many tokens we have
        nb_tokens = int(torch.sum(mask).item())

        # weighted Cross-entropy loss with the mask  as weights (ignore <PAD> tokens)
        y_pred = y_pred[range(y_pred.shape[0]), y] * mask
        ce_loss = -torch.sum(y_pred) / nb_tokens

        return ce_loss

    def train_model(self, train_loader, val_loader, optimizer, n_epochs, print_every_n_epoch=1,
                    display=False):

        """
        :binary: bool, indicates that the targets will be binary and that the autoencoder has a sigmoid as output.
        """

        train_loss_history = np.zeros(n_epochs)
        val_loss_history = np.zeros(n_epochs)
        self.to(self.device)

        for epoch in range(n_epochs):
            # training
            self.train()
            train_loss = 0

            for it, data in enumerate(train_loader):
                optimizer.zero_grad()
                self.zero_grad()
                hidden_enc = self.init_hidden(data.size()[0])
                hidden_dec = self.init_hidden(data.size()[1])
                data = data.to(self.device)
                optimizer.zero_grad()
                output = self.forward(data, hidden_enc, hidden_dec)
                loss = self.loss(output, data)
                loss.backward()
                optimizer.step()
                train_loss += loss.detach().cpu().numpy()

            n_train_batches = it
            train_loss_history[epoch] = train_loss / n_train_batches
            # evaluation
            val_loss = 0
            self.eval()
            for it, data in enumerate(val_loader):
                hidden_enc = self.init_hidden(data.size()[0])
                hidden_dec = self.init_hidden(data.size()[1])
                data = data.to(self.device)
                output = self.forward(data, hidden_enc, hidden_dec)
                loss = self.loss(output, data).detach().cpu().numpy()
                val_loss += loss

            n_val_batches = it
            val_loss_history[epoch] = val_loss / n_val_batches
            if (epoch + 1) % print_every_n_epoch == 0:
                print(
                    f'Epoch {epoch} training loss {train_loss / n_train_batches} validation loss {val_loss / it}')

        if display:
            plot_learning_history(train_loss_history, val_loss_history)
        self.eval()
        return train_loss_history, val_loss_history

    def evaluate(self, val_loader):

        self.eval()
        accuracy_list = []
        for it, data in enumerate(val_loader):
            hidden_enc = self.init_hidden(data.size()[0])
            hidden_dec = self.init_hidden(data.size()[1])
            data = data.to(self.device)
            output = self.forward(data, hidden_enc, hidden_dec)
            output = output.argmax(dim=2)
            # find indices of first stop token
            tmp = (output == self.stop_idx).float()
            idx = torch.arange(tmp.shape[1], 0, -1).float().to(self.device)
            tmp2 = tmp * idx
            indices = torch.argmax(tmp2, 1)
            indices = torch.where(indices != 0, indices,
                                  output.size()[1] * torch.ones(indices.size()).long().to(
                                      self.device))
            # withdraw elements after the first stop token
            indices_mask = torch.arange(output.size()[1]) * torch.ones(output.size())
            indices_mask = indices_mask.to(self.device)
            mask = indices_mask.long() <= indices.long().view(-1, 1).long()
            trunc_output = torch.where(mask, output,
                                       self.padding_idx * torch.ones(mask.size()).long().to(
                                           self.device))

            # compute accuracy
            # create a mask by filtering out all tokens that ARE NOT the padding token
            mask = (output != self.padding_idx).float()
            # count how many tokens we have
            nb_tokens = int(torch.sum(mask).item())
            # accuracy ignoring pad tokens
            acc = (output == data).float() * mask
            acc = acc.sum() / nb_tokens
            accuracy_list.append(acc)

        return np.mean(accuracy_list)
