from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch
from ...utils.torch_tools import plot_learning_history,generate_noise
from .FCGAN import FCGAN
from torch import autograd
import numpy as np 

class Discriminator_no_bn(nn.Module):

    def __init__(self, inp,hidden_dims):

        super().__init__()

        layer_dims = (inp,) + hidden_dims
        self.linear_layers = nn.ModuleList()
        self.dropout = nn.ModuleList()
        for k in range(len(layer_dims)-2):
            self.linear_layers.append(nn.Linear(layer_dims[k],layer_dims[k+1]))
            self.dropout.append(nn.Dropout(p=0.2))
        self.fc_clf_layer = nn.Linear(layer_dims[-2],layer_dims[-1])
        self.clf_layer = nn.Sigmoid()

    def forward(self, x):
        x = x.squeeze(1)
        for lin,do in zip(self.linear_layers,self.dropout):
            x = F.leaky_relu(do(lin(x)))
        x = self.fc_clf_layer(x)
        x = self.clf_layer(x)

        return x

def compute_gradient_penalty(D, real_samples, fake_samples,device):
    """Calculates the gradient penalty loss for WGAN GP"""
    # Random weight term for interpolation between real and fake samples
    alpha = torch.from_numpy(np.random.random((real_samples.size(0),1))).float().to(device)
    # Get random interpolation between real and fake samples
    interpolates = (alpha * real_samples + ((1 - alpha) * fake_samples)).requires_grad_(True)
    d_interpolates = D(interpolates)
    fake = Variable(torch.ones(real_samples.shape[0], 1), requires_grad=False).to(device)
    # Get gradient w.r.t. interpolates
    gradients = autograd.grad(
        outputs=d_interpolates,
        inputs=interpolates,
        grad_outputs=fake,
        create_graph=True,
        retain_graph=True,
        only_inputs=True,
    )[0]
    gradients = gradients.view(gradients.size(0), -1)
    gradient_penalty = ((gradients.norm(2, dim=1) - 1) ** 2).mean()
    return gradient_penalty

class WGAN_GP(FCGAN):
    """
    Variation on WGAN, where the Lipschitz constraint is not enforced
    through weights clipping but through gradient penalization
    (a 1-Lipschitz function should have <1-normed gradient almost everywhere)
    """

    def __init__(self,autoencoder,noise_dim,g_hidden_dims,d_hidden_dims,lambda_gp):
        super().__init__(autoencoder,noise_dim,g_hidden_dims,d_hidden_dims)
        #modify discriminator to remove batch normalisation
        self.discriminator = Discriminator_no_bn(self.encoding_dim,self.d_hidden_dims)
        self.lambda_gp = lambda_gp
        self.args['lambda_gp'] = lambda_gp

    def train_model(self, train_loader,g_optimizer,d_optimizer,n_epochs,
                    print_every_n_epochs, display = True):
        """
        Parameters
        ----------
        train_loader: torch DataLoader
        g_optimizer, d_optimizer: optimizers for generator and discriminator (resp.)
        n_epochs: int, number of epochs of training
        print_every_n_epoch: int
        display: bool, plot losses histories or not
        
        NB: 1 indicates real, 0 indicates fake
        """
        #put modules on device
        self.discriminator.to(self.device)
        self.generator.to(self.device)
        self.autoencoder.to(self.device)
        #store learning history
        gen_loss_hist = []
        disc_loss_hist = []

        self.generator.train(), self.discriminator.train()

        for epoch in range(n_epochs):
            g_epoch_loss = 0
            d_epoch_loss = 0
            epoch_accuracy = []
            for n_batch,real_batch in enumerate(train_loader):

                # Configure input
                real_batch = Variable(real_batch.to(self.device))
                N = real_batch.size(0)
                # ---------------------
                #  Train Discriminator
                # ---------------------
                d_optimizer.zero_grad()
                # Sample noise as generator input
                z = generate_noise(N,self.noise_dim).to(self.device)
                # Generate a batch of images
                fake_data = self.generator(z).detach()
                # Real images
                real_data_encoded = self.autoencoder.encode(real_batch)
                pred_real = self.discriminator(real_data_encoded)
                # Fake images
                pred_fake = self.discriminator(fake_data)
                # Gradient penalty
                gradient_penalty = compute_gradient_penalty(self.discriminator, real_data_encoded.data, fake_data.data,self.device)
                # Adversarial loss
                d_loss = -torch.mean(pred_real) + torch.mean(pred_fake) + self.lambda_gp * gradient_penalty
                d_loss.backward()
                d_optimizer.step()
                d_epoch_loss += d_loss.item()
                epoch_accuracy.append(1-pred_fake.round().mean().detach())
                epoch_accuracy.append(pred_real.round().mean().detach())
                #---------------
                #Train generator
                #---------------
                g_optimizer.zero_grad()
                # Generate a batch of images
                fake_data = self.generator(z)
                # Train on fake images
                pred_fake = self.discriminator(fake_data)
                g_loss = -torch.mean(pred_fake)
                g_loss.backward()
                g_optimizer.step()
                g_epoch_loss += g_loss.item()

            g_epoch_loss /= n_batch
            d_epoch_loss /= n_batch
            gen_loss_hist.append(g_epoch_loss)
            disc_loss_hist.append(d_epoch_loss)

            if (epoch+1) % print_every_n_epochs == 0:
                print(f'Epoch {epoch}: discriminator loss {d_epoch_loss}, generator loss {g_epoch_loss}, accuracy {np.mean(epoch_accuracy)}')

        if display:
            plot_learning_history(gen_loss_hist,disc_loss_hist,['generator','discriminator'])

        self.generator.eval(),self.discriminator.eval()

        return gen_loss_hist,disc_loss_hist
