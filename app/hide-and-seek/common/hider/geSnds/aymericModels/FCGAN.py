from torch import nn
import torch
import torch.nn.functional as F
from torch.autograd import Variable
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import train_test_split
import numpy as np
import matplotlib.pyplot as plt
from ...utils.torch_tools import plot_learning_history,generate_noise,ones_target,zeros_target
from torch.utils.data import DataLoader


class Generator(nn.Module):

    def __init__(self, inp, hidden_dims,device,residual=False):

        super().__init__()
        self.device = device
        self.input_dim = inp
        self.residual = residual
        self.layer_dims = (inp,) + hidden_dims
        self.linear_layers = nn.ModuleList()
        self.batchnorm = nn.ModuleList()
        self.dropout = nn.ModuleList()
        for k in range(len(self.layer_dims)-2):
            self.linear_layers.append(nn.Linear(self.layer_dims[k],self.layer_dims[k+1]))
            self.batchnorm.append(nn.BatchNorm1d(self.layer_dims[k+1],momentum=0.01 ))
            self.dropout.append(nn.Dropout(p=0.2))
        self.final_layer = nn.Linear(self.layer_dims[-2],self.layer_dims[-1])
        
            

    def forward(self, x):
        for k,(lin,bn,do) in enumerate(zip(self.linear_layers,self.batchnorm,self.dropout)):
            #insert residual block if possible
            if k<len(self.layer_dims) and self.residual and self.layer_dims[k]==self.layer_dims[k+1]:
                x = F.leaky_relu(do(bn(lin(x)))) + x
            else:
                x = F.leaky_relu(do(bn(lin(x))))
        x = self.final_layer(x)
        x = F.sigmoid(x)
        return x
    
    def sample(self,n):
        random_noise = generate_noise(n,self.input_dim)
        return self.forward(random_noise.to(self.device))
    

class Discriminator(nn.Module):

    def __init__(self, inp,hidden_dims):

        super().__init__()

        layer_dims = (inp,) + hidden_dims
        self.linear_layers = nn.ModuleList()
        self.batchnorm = nn.ModuleList()
        self.dropout = nn.ModuleList()
        for k in range(len(layer_dims)-2):
            self.linear_layers.append(nn.Linear(layer_dims[k],layer_dims[k+1]))
            self.batchnorm.append(nn.BatchNorm1d(layer_dims[k+1]))
            self.dropout.append(nn.Dropout(p=0.2))
        self.fc_clf_layer = nn.Linear(layer_dims[-2],layer_dims[-1])
        self.clf_layer = nn.Sigmoid()

    def forward(self, x):
        x = x.squeeze(1)
        for lin,bn,do in zip(self.linear_layers,self.batchnorm,self.dropout):
            x = F.leaky_relu(do(bn(lin(x))))
        x = self.fc_clf_layer(x)
        x = self.clf_layer(x)
        
        return x

    


class FCGAN():
    """
    generates in latent space, and a pretrained autoencoder (which remains untouched) decodes the generations
    """
    
    def __init__(self,autoencoder,noise_dim,g_hidden_dims,d_hidden_dims):
        """
        parameters
        ----------
        autoencoder : a pretrained object of the class AE 
        noise_dim : dimension of the noise from which the generator builds samples
        g_hidden_dims : tuple, dimension of the hidden layers of the generator (w/o input and output layers)
        d_hidden_dims : tuple, dimension of the hidden layers of the discriminator (w/o input and output layers)
        """
    
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        #for saving and loading purposes
        args = locals()
        self.args = {k:v for k,v in args.items() if not k in ['self','autoencoder','__class__']}
        #dimensions
        self.noise_dim = noise_dim
        self.input_dim = autoencoder.encoding[0].in_features
        self.encoding_dim = autoencoder.encoding[-1].out_features
        self.g_hidden_dims = g_hidden_dims + (self.encoding_dim,) #append output dimension for generator
        self.d_hidden_dims=  d_hidden_dims + (1,) #append output dimension for discriminator
        #modules
        self.autoencoder = autoencoder
        self.discriminator = Discriminator(self.encoding_dim,self.d_hidden_dims)
        self.generator = Generator(noise_dim,self.g_hidden_dims,self.device)
        

    def train_model(self,train_loader,g_optimizer,d_optimizer, n_epochs,
                    label_smoothing = 0.9,print_every_n_epoch=1,display=False):
        """
        Parameters
        ----------
        train_loader: torch DataLoader
        g_optimizer, d_optimizer: optimizers for generator and discriminator (resp.)
        n_epochs: int, number of epochs of training
        label_smoothing: float, 0<x<1. When training discriminator, the targets are label_smoothing*1. instead of 1 for real data
        print_every_n_epoch: int
        display: bool, plot losses histories or not
        
        NB: 1 indicates real, 0 indicates fake
        """

        self.autoencoder.to(self.device)
        self.generator.to(self.device)
        self.discriminator.to(self.device)
        #define loss 
        loss = nn.BCELoss()

        g_loss_history = np.zeros(n_epochs)
        d_loss_history = np.zeros(n_epochs)
        self.generator.train()
        self.discriminator.train()

        for epoch in range(n_epochs):
            generator_loss = 0
            discriminator_loss = 0
            epoch_accuracy = []
            for n_batch, real_batch in enumerate(train_loader):
                
                real_batch = real_batch.to(self.device)
                N = real_batch.size(0)
                
                #----------------
                # Train generator
                #----------------
                
                #generate fake data
                fake_data = self.generator.sample(N)
                #compute error
                g_optimizer.zero_grad()
                prediction = self.discriminator(fake_data)
                g_error = loss(prediction, ones_target(N).to(self.device))
                g_error.backward()
                g_optimizer.step()
                
                #--------------------
                # Train discriminator
                #--------------------
                
                real_data = self.autoencoder.encode(real_batch)
                # Generate fake data and detach
                fake_data = self.generator.sample(N).detach()
                ##train discriminator
                d_optimizer.zero_grad()
                #error on real data
                prediction_real = self.discriminator(real_data)
                error_real = loss(prediction_real, label_smoothing * ones_target(N).to(self.device) )
                #error on fake data
                prediction_fake = self.discriminator(fake_data)
                error_fake = loss(prediction_fake, zeros_target(N).to(self.device))
                d_error = error_real + error_fake
                d_error.backward()
                d_optimizer.step()
                #update losses
                generator_loss += g_error.detach().cpu().numpy()
                discriminator_loss += d_error.detach().cpu().numpy()
                epoch_accuracy.append(1-prediction_fake.round().mean().detach())
                epoch_accuracy.append(prediction_real.round().mean().detach())

            g_loss_history[epoch] = generator_loss/n_batch
            d_loss_history[epoch] = discriminator_loss/n_batch

            if (epoch+1) % print_every_n_epoch == 0:
                print(f'Epoch {epoch} generator loss {generator_loss/n_batch} discriminator loss {discriminator_loss/n_batch} accuracy {np.mean(epoch_accuracy)}')

        if display:
            plot_learning_history(g_loss_history,d_loss_history,['generator','discriminator'])
        self.generator.eval(),self.discriminator.eval()
        
        return g_loss_history,d_loss_history
    
    def evaluate_model(self,n_samples,validation_set):
        """
        Compute accuracy on real unseen data and synthetic data
        
        Parameters
        ----------
        n_samples: int, number of samples to generate, and number of samples to take from validation_set
        validation_set: BOEDataset instance
        
        Returns
        -------
        acc: dict with keys 'accuracy_real' and 'accuracy_fake'
        """
        #compute accuracy on fake data
        samples = self.generator.sample(n_samples)
        predictions = self.discriminator(samples)
        predictions = predictions.round().detach().cpu().numpy()
        accuracy_fake = np.mean(predictions==0)

        #compute accuracy on real data
        loader = DataLoader(validation_set,shuffle=True,batch_size=n_samples)
        samples = next(iter(loader))
        #push in latent space
        encoded = self.autoencoder.encode(samples.to(self.device))
        predictions = self.discriminator(encoded).round().detach().cpu().numpy()
        accuracy_real = np.mean(predictions==1)
        acc = {'accuracy_real':accuracy_real,'accuracy_fake':accuracy_fake}
        return acc

    def sample(self,n_samples,decode=True):
        """
        Parameters
        ----------
        n_samples: int, number of samples to generate
        decode: bool (default True), if True the samples are in the original space, otherwise they are in the latent space
        """
        sample = self.generator.sample(n_samples)
        if decode:
            sample = self.autoencoder.decode(sample).round()
        return sample.detach().cpu().numpy()




    
    