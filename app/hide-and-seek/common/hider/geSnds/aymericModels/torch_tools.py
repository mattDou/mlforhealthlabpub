import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn.functional as F
from torch import nn
from torch.autograd import Variable
from torch.utils.data import Dataset


class AmdbSequencesDataset(Dataset):
    """
    Dataset for sequences of the amdb data
    """

    def __init__(self, data):
        super(AmdbSequencesDataset, self).__init__()
        self.data = data

    def __getitem__(self, index):
        self.data.loc[index]
        return torch.from_numpy(self.data.loc[index])

class SparseDataset(Dataset):
    """
    Dataset which enables to load sparse matrices batch-wise as arrays
    (to avoid using too much memory during training)
    """

    def __init__(self, data):
        super(SparseDataset, self).__init__()
        self.data = data

    def __getitem__(self, index):
        return torch.from_numpy(self.data[index, :].todense()).float()

    def __len__(self):
        return self.data.shape[0]


class BOEDataset(Dataset):
    """
    Dataset for bag of events
    """

    def __init__(self, df, cv):
        """
        df: pandas DataFrame containing the data. Must have a 'slided_events_join' column
        cv: sklearn CountVectorizer, pretrained
        """
        super(BOEDataset, self).__init__()
        self.df = df
        self.cv = cv

    def __len__(self):
        return self.df.shape[0]

    def __getitem__(self, index):
        sequence = self.df['slided_events_join'].iloc[index]
        boe = self.cv.transform([sequence])

        return torch.from_numpy(boe.todense()).float()


class EventsSequencesDataset(Dataset):
    """
    Dataset for sequences of events. Handles the conversion form codes to indices, the padding and the insertion of start and stop tokens
    """

    def __init__(self, df, vocab, padding_idx=0, autoencoding=False):
        """
        :param (pd.DataFrame) df: dataframe containing the data. Must have a 'slided_events_join' column
        :param (dict) vocab: vocabulary mapping medical codes to indices
        :param (int) padding_idx: index of the pad token
        :param (bool) autoencoding: if True, the getitem method only returns a sequence. 
                                    Otherwise, it returns two sequences (the second being the first one shifted of one timestep) 
        """
        super().__init__()
        self.vocab = vocab
        self.df = df
        self.maxlen = np.max([len(seq.split()) for seq in df['slided_events_join']]) + 2
        self.padding_idx = padding_idx
        self.autoencoding = autoencoding

    def __len__(self):
        return len(self.df)

    def __getitem__(self, index):
        seq = ['<sos>'] + self.df['slided_events_join'].iloc[index].split() + ['<eos>']

        encoded_seq = np.array([self.vocab[s] for s in seq])
        if not self.autoencoding:
            y = encoded_seq[1:]
            x = encoded_seq[:-1]
            x = np.concatenate((x, self.padding_idx * np.ones(self.maxlen - len(x))))
            y = np.concatenate((y, self.padding_idx * np.ones(self.maxlen - len(y))))

            return torch.from_numpy(x).long(), torch.from_numpy(y).long()
        else:
            x = encoded_seq
            x = np.concatenate((x, self.padding_idx * np.ones(self.maxlen - len(x))))
            return torch.from_numpy(x).long()


class FakeRealSequencesDataset(Dataset):
    """
    Dataset containing both real and synthetic sequences
    """

    def __init__(self, real_sequences, fake_sequences, padding_idx, prediction_type):

        """
        Parameters
        ----------
        real_sequences: array-like of sequences
        fake_sequences: array-like of sequences
        padding_idx: int, index of the pad token
        prediction_type: str, 'binary' or 'next'. If 'binary', the getitem method returns a sequence and its label (1 for real and 0 for fake)
                        If 'next', the getitem method returns a sequence and the next event
        """
        super().__init__()
        assert (prediction_type in ['binary', 'next'])
        self.real_sequences = real_sequences
        self.fake_sequences = fake_sequences
        self.padding_idx = padding_idx
        if len(real_sequences) == 0:
            self.maxlen = np.max([len(s) for s in fake_sequences]) + 2
        elif len(fake_sequences) == 0:
            self.maxlen = np.max([len(s) for s in real_sequences]) + 2
        else:
            self.maxlen = max(np.max([len(s) for s in real_sequences]),
                              np.max([len(s) for s in
                                      fake_sequences])) + 2  # for start and stop tokens
        self.prediction_type = prediction_type

    def __len__(self):
        return len(self.real_sequences) + len(self.fake_sequences)

    def __getitem__(self, index):

        if index < len(self.real_sequences):
            seq = self.real_sequences[index]
        else:
            seq = self.fake_sequences[index - len(self.real_sequences)]
        if self.prediction_type == 'binary':
            label = int(index < len(self.real_sequences))

        elif self.prediction_type == 'next':
            seq = seq[:-1]
            label = seq[-1]

        x = np.concatenate((seq, self.padding_idx * np.ones(self.maxlen - len(seq))))
        if self.prediction_type == 'next':
            label = label.reshape(-1, 1)
            return torch.from_numpy(x).long(), torch.from_numpy(label).long().squeeze()
        else:
            return torch.from_numpy(x).long(), label


def cosine_mse_loss(ytrue, ypred, wcs=0.5, wmse=0.5):
    """
    Combines -cosine similarity and mean squared error
    wcs: weight of the cosine similarity
    wmse: weight of the MSE
    """
    return -wcs * F.cosine_similarity(ytrue, ypred).mean() + wmse * nn.MSELoss()(ytrue, ypred)


def plot_learning_history(history1, history2, labels=['train loss', 'validation loss']):
    fig, ax = plt.subplots(1)
    ax.plot(history1, label=labels[0], c='b')
    ax.plot(history2, label=labels[1], c='orange')
    ax.set_title('Evolution of losses')
    ax.legend()
    plt.show()


"""
AUXILIARY FUNCTIONS FOR GAN TRAINING
"""


def generate_noise(size, noise_dim):
    '''
    Generates a 1-d vector of gaussian sampled random values
    '''
    n = Variable(torch.randn(size, noise_dim)).squeeze()
    return n


def ones_target(size):
    '''
    Tensor containing ones, with shape = size
    '''
    data = Variable(torch.ones(size, 1)).squeeze()
    return data


def zeros_target(size):
    '''
    Tensor containing zeros, with shape = size
    '''
    data = Variable(torch.zeros(size, 1)).squeeze()
    return data
