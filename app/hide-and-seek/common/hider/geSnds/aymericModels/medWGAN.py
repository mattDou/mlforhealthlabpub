from torch import nn
import torch
import torch.nn.functional as F
from torch.autograd import Variable
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import train_test_split
import numpy as np
import matplotlib.pyplot as plt
from ...utils.torch_tools import plot_learning_history,generate_noise,ones_target,zeros_target
from torch.utils.data import DataLoader
from .FCGAN import FCGAN, Discriminator
from collections import OrderedDict
from .medGAN import MedGAN

class MedWGAN(MedGAN):
    """
    generates in the latent space, autoencoder decodes 
    the block generator+autoencoder is trained simultaneously
    the discriminator works on data in the original space
    But not classical GAN: Wasserstein GAN
    """
    
    def __init__(self,autoencoder,noise_dim,g_hidden_dims,d_hidden_dims,num_words,clip_value):
        super().__init__(autoencoder,noise_dim,g_hidden_dims,d_hidden_dims,num_words)
        self.clip_value = clip_value
        self.args['clip_value'] = clip_value
        
    def train_model(self,train_loader,g_optimizer,d_optimizer, dec_optimizer,
                    n_epochs,label_smoothing = 0.9,print_every_n_epoch=1,display=False):
        """
        parameters
        ----------
        train_loader: torch DataLoader
        g_optimizer, d_optimizer,dec_optimizer: optimizers for generator, discriminator and autoencoder (resp.)
        n_epochs: int, number of epochs of training
        label_smoothing: float, 0<x<1. When training discriminator, the targets are label_smoothing*1. instead of 1 for real data
        print_every_n_epoch: int
        display: bool, if True plot learning history otherwise not
        
        NB: 1 indicates real, 0 indicates fake
        """

        self.autoencoder.to(self.device)
        self.generator.to(self.device)
        self.discriminator.to(self.device)

        g_loss_history = np.zeros(n_epochs)
        d_loss_history = np.zeros(n_epochs)
        self.generator.train()
        self.discriminator.train()
        self.autoencoder.train()

        for epoch in range(n_epochs):
            generator_loss = 0
            discriminator_loss = 0
            epoch_accuracy = []
            for n_batch, real_batch in enumerate(train_loader):
                
                real_batch = real_batch.to(self.device)
                N = real_batch.size(0)
                #----------------
                # Train generator
                #----------------
                #generate fake data
                fake_data = self.generator.sample(N)
                fake_data = self.autoencoder.decode(fake_data)
                #compute error
                g_optimizer.zero_grad()
                gdec_error = -torch.mean(self.discriminator(fake_data))
                gdec_error.backward()
                g_optimizer.step()
                dec_optimizer.step()
                #--------------------
                # Train discriminator
                #--------------------
                # Generate fake data and detach
                fake_data = self.generator.sample(N).detach()
                fake_batch = self.autoencoder.decode(fake_data).detach()
                #zero grad
                d_optimizer.zero_grad()
                #error on real data
                prediction_real = self.discriminator(real_batch)
                #error on fake data
                prediction_fake = self.discriminator(fake_batch)
                d_error = torch.mean(prediction_fake)-torch.mean(prediction_real)
                d_error.backward()
                d_optimizer.step()
                # Clip weights of discriminator
                for p in self.discriminator.parameters():
                    p.data.clamp_(-self.clip_value, self.clip_value)
                #update losses
                generator_loss += gdec_error.detach().cpu().numpy()
                discriminator_loss += d_error.detach().cpu().numpy()
                #store accuracy
                epoch_accuracy.append(1-prediction_fake.round().mean().detach())
                epoch_accuracy.append(prediction_real.round().mean().detach())

            g_loss_history[epoch] = generator_loss/n_batch
            d_loss_history[epoch] = discriminator_loss/n_batch

            if (epoch+1) % print_every_n_epoch == 0:
                print(f'Epoch {epoch} generator loss {generator_loss/n_batch} discriminator loss {discriminator_loss/n_batch},accuracy {np.mean(epoch_accuracy)}')

        if display:
            plot_learning_history(g_loss_history,d_loss_history,['generator','discriminator'])
        self.generator.eval(),self.discriminator.eval(),self.autoencoder.eval()
        
        return g_loss_history,d_loss_history