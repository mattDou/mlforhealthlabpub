from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch
from ...utils.torch_tools import plot_learning_history,generate_noise
from .FCGAN import FCGAN
import numpy as np


class WGAN(FCGAN):
    """
    Wasserstein GAN
    Objective is not KL div anymore but approximation of earth mover distance
    """
    def __init__(self,autoencoder,noise_dim,g_hidden_dims,d_hidden_dims,
                clip_value):
        """
        Parameters
        ----------
        clip_value : float, value to which the discriminator weights are clipped.
                     The clipping is used to enforce a Lipschitz constraint
        """
        super().__init__(autoencoder,noise_dim,g_hidden_dims,d_hidden_dims)
        self.clip_value = clip_value
        self.args['clip_value'] = clip_value

    def train_model(self,train_loader,g_optimizer,d_optimizer,n_epochs,
                    print_every_n_epochs, display = True):
        """
        Parameters
        ----------
        train_loader: torch DataLoader
        g_optimizer, d_optimizer: optimizers for generator and discriminator (resp.)
        n_epochs: int, number of epochs of training
        print_every_n_epoch: int
        display: bool, plot losses histories or not
        
        NB: 1 indicates real, 0 indicates fake
            optimizers should be RMSprop and not Adam
        """
        #put modules on device
        self.discriminator.to(self.device)
        self.generator.to(self.device)
        self.autoencoder.to(self.device)
        #store learning history
        gen_loss_hist = []
        disc_loss_hist = []

        self.generator.train(), self.discriminator.train()
        for epoch in range(n_epochs):
            g_epoch_loss = 0
            d_epoch_loss = 0
            epoch_accuracy = []
            for n_batch,real_batch in enumerate(train_loader):
                real_batch = Variable(real_batch).to(self.device)
                N = real_batch.size(0)
                #-------------------
                #Train discriminator
                #-------------------
                d_optimizer.zero_grad()
                #project batch in the encoding space
                real_data_encoded = self.autoencoder.encode(real_batch)
                z = generate_noise(N,self.noise_dim).to(self.device)
                fake_data = self.generator(z).detach()
                prediction_real = self.discriminator(real_data_encoded)
                prediction_fake = self.discriminator(fake_data)
                loss_D = torch.mean(prediction_fake)-torch.mean(prediction_real)
                loss_D.backward()
                d_optimizer.step()
                d_epoch_loss += loss_D.item()
                # Clip weights of discriminator
                for p in self.discriminator.parameters():
                    p.data.clamp_(-self.clip_value, self.clip_value)
                #store accuracy
                epoch_accuracy.append(1-prediction_fake.round().mean().detach())
                epoch_accuracy.append(prediction_real.round().mean().detach())
                #---------------
                #Train generator
                #---------------
                g_optimizer.zero_grad()

                # Generate a batch
                fake_data = self.generator(z)
                # Adversarial loss
                loss_G = -torch.mean(self.discriminator(fake_data))
                loss_G.backward()
                g_optimizer.step()
                g_epoch_loss += loss_G.item()

            g_epoch_loss /= n_batch
            d_epoch_loss /= n_batch
            gen_loss_hist.append(g_epoch_loss)
            disc_loss_hist.append(d_epoch_loss)

            if (epoch+1) % print_every_n_epochs == 0:
                print(f'Epoch {epoch}: discriminator loss {d_epoch_loss}, generator loss {g_epoch_loss}, accuracy {np.mean(epoch_accuracy)}')

        if display:
            plot_learning_history(gen_loss_hist,disc_loss_hist,['generator','discriminator'])

        self.generator.eval(),self.discriminator.eval()

        return gen_loss_hist,disc_loss_hist
