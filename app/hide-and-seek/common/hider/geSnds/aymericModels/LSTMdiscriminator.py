import numpy as np
import torch
import torch.nn as nn
from sklearn.metrics import classification_report

from hider.aymericModels.torch_tools import plot_learning_history


class LSTMClassifier(nn.Module):
    """
    used to discriminate sequences between real and synthetic
    """

    def __init__(
            self,
            hidden_dim,
            embedding_dim,
            vocab_size,
            prediction_layer,
            n_layers=1,
            prediction_type='binary',
            device=None
    ):

        super().__init__()
        if device is None:
            self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        else:
            self.device = device

        assert prediction_type in ['binary', 'next']
        self.prediction_type = prediction_type
        self.prediction_layer = prediction_layer
        # dims
        self.hidden_dim = hidden_dim
        self.n_layers = n_layers
        self.vocab_size = vocab_size
        # layers
        self.embedding = nn.Embedding(vocab_size, embedding_dim)
        self.lstm = nn.LSTM(embedding_dim, hidden_dim, num_layers=self.n_layers, dropout=0.5,
                            batch_first=True)
        self.dropout = nn.Dropout(0.5)
        if prediction_type == 'binary':
            output_size = 1
        elif prediction_type == 'next':
            output_size = vocab_size
        self.clf_layer = nn.Linear(hidden_dim, output_size)
        self.sigmoid = nn.Sigmoid()

    def forward(self, x, hidden):
        batch_size, seq_len = x.size()
        embedded = self.embedding(x)
        lstm_out, hidden = self.lstm(embedded, hidden)
        out = self.dropout(lstm_out)
        if self.prediction_layer == 'last_hidden':
            context_vector = hidden[0].squeeze()
        elif self.prediction_layer == 'max_pooling':
            #  max_pool on outputs along temporal dimension
            context_vector = out.max(dim=1)[0]
        elif self.prediction_layer == 'mean_pooling':
            #  mean_pool on outputs along temporal dimension
            context_vector = (out.sum(dim=1) / seq_len).squeeze()
        out = self.clf_layer(context_vector)
        out = self.sigmoid(out)
        if self.prediction_type == 'next':
            out = out.view(batch_size, self.vocab_size)
        elif self.prediction_type == 'binary':
            out = out.view(batch_size, -1)
        return out, hidden

    def init_hidden(self, batch_size):

        weight = next(self.parameters()).data
        hidden = (weight.new(self.n_layers, batch_size, self.hidden_dim).zero_().to(self.device),
                  weight.new(self.n_layers, batch_size, self.hidden_dim).zero_().to(self.device))
        return hidden

    def train_model(self, train_loader, val_loader, optimizer, n_epochs, print_every_n_epochs=1,
                    display=False):

        if self.prediction_type == 'binary':
            criterion = nn.BCELoss()
        elif self.prediction_type == 'next':
            criterion = nn.NLLLoss()

        train_hist = {'loss': [], 'accuracy': []}
        val_hist = {'loss': [], 'accuracy': []}
        self.to(self.device)
        for epoch in range(n_epochs):
            hidden = self.init_hidden(train_loader.batch_size)
            train_loss = 0
            self.train()
            for it_train, (seq, target) in enumerate(train_loader):
                optimizer.zero_grad()
                seq, target = seq.to(self.device), target.to(self.device)
                if self.prediction_type == 'binary':
                    target = target.float()
                output, _ = self.forward(seq, hidden)
                loss = criterion(output, target)
                loss.backward()
                optimizer.step()

                train_loss += loss.detach().cpu().numpy()

            hidden = self.init_hidden(val_loader.batch_size)
            with torch.no_grad():
                self.eval()
                for it_val, (seq, target) in enumerate(val_loader):
                    val_loss = 0
                    with torch.no_grad():
                        seq, target = seq.to(self.device), target.to(self.device)
                        if self.prediction_type == 'binary':
                            target = target.float()
                        output, _ = self.forward(seq, hidden)

                        loss = criterion(output, target)
                        val_loss += loss.detach().cpu().numpy()

                train_hist['loss'].append(train_loss / it_train)
                val_hist['loss'].append(val_loss / it_val)

        if display:
            plot_learning_history(train_hist['loss'], val_hist['loss'], ['train', 'validation'])

    def evaluate(self, val_loader):

        eval_model = self.to(self.device).eval()
        predictions = np.zeros(0)
        ground_truth = np.zeros(0)
        with torch.no_grad():
            for batch, labels in val_loader:
                batch = batch.to(self.device)
                hidden = eval_model.init_hidden(batch.size()[0])
                pred, _ = eval_model(batch, hidden)
                if self.prediction_type == 'binary':
                    predictions = np.concatenate(
                        (predictions, pred.round().squeeze().cpu().numpy()))
                elif self.prediction_type == 'next':
                    predictions = np.concatenate((predictions, pred.argmax(dim=1).cpu().numpy()))
                ground_truth = np.concatenate((ground_truth, labels.cpu().numpy()))
        if self.prediction_type == 'binary':
            report = classification_report(ground_truth, predictions, target_names=['fake', 'real'])
            return report

        elif self.prediction_type == 'next':
            accuracy = np.mean(ground_truth == predictions)
            return accuracy
