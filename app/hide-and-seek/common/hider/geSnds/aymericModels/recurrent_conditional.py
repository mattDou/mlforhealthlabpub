import json

import matplotlib.pyplot as plt
import numpy as np
import time as time
import torch
import torch.nn as nn

from hider.aymericModels.torch_tools import plot_learning_history


def gumbel_sample(size):
    uniforms = torch.rand(size)
    gumbel = -torch.log(-torch.log(uniforms))
    return gumbel


class ConditionalRnn(nn.Module):
    def __init__(
            self,
            vocab,
            pad_token,
            device,
            pretrained_embeddings_path=None,
            trainable_embeddings=True,
            embedding_dim=100,
            condition_dim=100,
            n_layers=1,
            hidden_dim=128,
            dropout=0.5,
            dataloader_nbworkers=8,
            verbose: int = 1):

        super().__init__()

        self.device = device
        self.vocab = vocab
        self.vocabulary_size = len(vocab)
        self.padding_idx = self.vocab[pad_token]
        self.stop_idx = self.vocab['<eos>']
        self.pretrained_embeddings_path = pretrained_embeddings_path
        self.trainable_embeddings = trainable_embeddings
        self.embedding_dim = embedding_dim
        self.condition_dim = condition_dim
        self.hidden_dim = hidden_dim
        self.n_layers = n_layers
        self.dropout = dropout
        self.dataloader_nbworkers = dataloader_nbworkers

        self.embeddings = self.init_embeddings(verbose=verbose)

        # linear layer to convert condition to hidden state shape
        self.cond2hidden = nn.Linear(self.condition_dim, self.hidden_dim)
        self.cond2cell = nn.Linear(self.condition_dim, self.hidden_dim)
        # dropout layer for regularization
        self.drop = nn.Dropout(self.dropout)
        self.bn1 = nn.BatchNorm1d(self.hidden_dim)

        # recurrent unit layer (lstm because this is robust, but could try with GRU cells)
        self.recurrent = nn.LSTM(
            input_size=self.embedding_dim,
            hidden_size=self.hidden_dim,
            num_layers=n_layers,
            batch_first=True)

        # map the hidden state to the vocabulary size to predict next word
        self.hidden2voca = nn.Linear(self.hidden_dim, self.vocabulary_size)
        self.sep_softmax = nn.Softmax(dim=1)
        # for saving and loading purposes
        args = locals()
        self.args = {k: v for k, v in args.items() if not k in ['self', '__class__']}

    def init_embeddings(self, verbose: int = 1):
        if self.pretrained_embeddings_path is not None:
            with open(self.pretrained_embeddings_path, 'r') as f:
                pretrained_embeddings_dict = json.load(f)
            pretrained_dim = len(list(pretrained_embeddings_dict.values())[0])
            assert pretrained_dim == self.embedding_dim, \
                'Pretrained Embeddings should be of the same dimension as model embedding dimension, catch {} but expected {}'.format(
                    pretrained_dim, self.embedding_dim)
            weights_matrix = np.zeros((len(self.vocab), self.embedding_dim))
            nb_replaced_embeddings = 0
            for code, ix in self.vocab.items():
                if code in pretrained_embeddings_dict.keys():
                    weights_matrix[ix] = pretrained_embeddings_dict[code]
                    nb_replaced_embeddings += 1
                else:
                    weights_matrix[ix] = np.random.normal(scale=0.6, size=(self.embedding_dim,))
            # emb_layer = nn.Embedding(self.vocabulary_size, self.embedding_dim)
            embeddings = nn.Embedding.from_pretrained(torch.Tensor(weights_matrix))
            if verbose:
                print('{}/{} codes in voca match pretrained embeddings'.format(
                    nb_replaced_embeddings, len(self.vocab)))
        else:
            embeddings = nn.Embedding(
                num_embeddings=self.vocabulary_size,
                embedding_dim=self.embedding_dim,
                padding_idx=self.padding_idx)
        # trainable or frozen embeddings ?
        embeddings.weight.requires_grad = self.trainable_embeddings
        if verbose:
            print("Trainable embeddings : {}".format(self.trainable_embeddings))
        return embeddings

    def init_hidden(self, batch_size, condition):
        # we initialize the hidden and cell states with  trainable layers
        # to operate the conditioning
        # The axes semantics are (num_layers, minibatch_size, hidden_dims)
        hidden_cond = self.cond2hidden(condition)
        cell_cond = self.cond2cell(condition)
        hidden_cond = torch.repeat(hidden_cond, self.n_layers)
        cell_cond = torch.repeat(cell_cond, self.n_layers)
        hidden = (hidden_cond, cell_cond)

        return hidden

    def forward(self, batch, hidden):
        batch_size, seq_len = batch.size()
        # embedding : (batch_size, seq_len, 1) -> (batch_size, seq_len, embedding_dim)
        embeds = self.embeddings(batch)
        embeds_dropped = self.drop(embeds)
        # pack_padded_sequence to hide padded items to the recurrent layer
        rec_out, hidden = self.recurrent(embeds_dropped, hidden)
        rec_out = rec_out.contiguous()
        rec_out_resized = rec_out.view(-1, self.hidden_dim)
        out = self.hidden2voca(rec_out_resized)
        # log-softmax
        probs = self.sep_softmax(out)
        # consider using adaptive soft max for imbalance distribution over outputs:
        # https://pytorch.org/docs/stable/nn.html?highlight=logsoftmax#torch.nn.AdaptiveLogSoftmaxWithLoss

        #  (batch_size * seq_len, nb_hidden) -> (batch_size, seq_len, vocabulary_size)
        return probs.view(batch_size, seq_len, self.vocabulary_size), hidden

    def loss(self, y_pred, y):
        """A loss function that deals with padded items and multi-task learning"""

        y = y.contiguous().view(-1)
        # flatten all predictions
        y_pred = y_pred.contiguous().view(-1, self.vocabulary_size)
        # create a mask by filtering out all tokens that ARE NOT the padding token
        tag_pad_token = self.padding_idx
        mask = (y != tag_pad_token).float()
        # count how many tokens we have
        nb_tokens = int(torch.sum(mask).item())

        # weighted Cross-entropy loss with the mask  as weights (ignore <PAD> tokens)
        y_pred = y_pred[range(y_pred.shape[0]), y] * mask
        ce_loss = -torch.sum(y_pred) / nb_tokens

        return ce_loss

    def train_model(
            self,
            train_loader,
            val_loader,
            optimizer,
            scheduler=None,
            nb_epochs=10,
            print_every_n_epochs=1,
            display=False
    ):

        # initialise history
        train_loss_history = []
        validation_loss_history = []
        train_total_loss = 0.
        dev_total_loss = 0.

        self.train()
        start_time = time.time()

        # TODO: let epoch be a parameter of the network
        epoch = 0
        batch_size = train_loader.batch_size
        for epoch in range(nb_epochs):
            epoch_start = time.time()
            train_epoch_loss = 0.
            # setup the model in train mode
            self.train()
            for i, (local_batch, local_cond, local_labels) in enumerate(
                    train_loader):
                optimizer.zero_grad()
                self.zero_grad()

                x = local_batch.long().to(self.device)
                y = local_labels.long().to(self.device)
                hidden = self.init_hidden(batch_size, local_cond)
                y_pred, _ = self.forward(x, hidden)
                loss = self.loss(y_pred, y)

                loss.backward()
                optimizer.step()
                train_epoch_loss += loss.item()
            n_train_batch = i

            dev_epoch_loss = 0.
            # Setup the model in eval mode (does not compute gradients)
            self.eval()
            with torch.no_grad():
                for i, (local_batch, local_labels, local_cond) in enumerate(
                        val_loader):
                    x = local_batch.long().to(self.device)
                    y = local_labels.long().to(self.device)
                    hidden = self.init_hidden(batch_size, local_cond)

                    y_pred, _ = self.forward(x, hidden)
                    loss = self.loss(y_pred, y)

                    dev_epoch_loss += loss
                n_val_batch = i

            if not scheduler is None:
                scheduler.step(dev_epoch_loss)

            train_total_loss += train_epoch_loss
            dev_total_loss += dev_epoch_loss
            train_loss_history.append(train_epoch_loss / n_train_batch)
            validation_loss_history.append(dev_epoch_loss / n_val_batch)

            if epoch % print_every_n_epochs == 0:
                print(
                    "\n Epoch {:.0f}, Train epoch Loss = {:.4f}, Dev epoch Loss = {:.4f}, time elapsed: {:.4f}".format(
                        epoch,
                        train_epoch_loss / n_train_batch,
                        dev_epoch_loss / n_val_batch,
                        time.time() - epoch_start)
                )
                # print("Total train Loss: {}".format(train_total_loss))

        print("------------------------------------------------")
        print(
            "\n Training ended, \n Total train Loss = {:.4f}, Total dev Loss = {:.4f}, total time elapsed: {:.4f}".format(
                train_total_loss, dev_total_loss, time.time() - start_time))
        if display:
            plot_learning_history(train_loss_history, validation_loss_history)

        return train_loss_history, validation_loss_history, epoch

    def evaluate(self,
                 val_loader,
                 k=1,
                 eval_device: str = 'cpu',
                 verbose=True):

        # not enough memory on gpu to keep the whole data set as tensors
        eval_model = self.to(eval_device)
        eval_model.eval()
        accuracies = []
        with torch.no_grad():
            for i, (batch, labels, cond) in enumerate(
                    val_loader):

                batch = batch.to(eval_device)
                labels = labels.to(eval_device)
                hidden = eval_model.init_hidden(batch.size()[0], cond)

                batch_y_pred, hidden = eval_model.forward(batch, hidden)
                batch_topks = batch_y_pred.topk(k, dim=2)[1]
                binary_accu = torch.zeros(batch.size()).to(eval_device)
                for i in range(k):
                    binary_accu += (batch_topks[:, :, i] == labels).float()
                binary_accu = (binary_accu > 0).float()
                mask = (labels != self.padding_idx).float()
                binary_accu *= mask
                accuracy = binary_accu.sum() / mask.sum()
                accuracies.append(accuracy)

        return np.mean(accuracies)

    def evaluate_every_k(self, val_loader, k_max, eval_device: str = 'cpu'):
        accuracy_list = np.zeros(k_max)
        for k in range(k_max):
            accuracy_list[k] = self.evaluate(val_loader, k + 1, eval_device, verbose=False)
        fig, ax = plt.subplots(1, 1)
        ax.plot(np.arange(1, k_max + 1), accuracy_list, marker='o')
        ax.set_title('Accuracy in top k')
        ax.set_xlabel(f'k={k_max}')
        plt.show()

        return accuracy_list

    def sample(self,
               starts,
               conds,
               n_steps_max,
               gumbel_max_trick=True,
               device: str = 'cpu'):

        """
        parameters
        ----------
        starts: torch Tensor of shape (n_samples,n_steps,vocab_size)
        n_steps_max: int, numbr of steps to sample
        gumbel_max_trick: bool, whether to implement the gumbel max trick while sampling (enhances diversity)

        returns
        -------
        padded_starts: torch Tensor of shape (n_samples,n_steps_max,vocab_size)
        """

        padded_starts = nn.utils.rnn.pad_sequence(starts, padding_value=self.padding_idx,
                                                  batch_first=True)
        hidden = self.init_hidden(len(starts), conds)
        it = 0
        while it < n_steps_max:
            out, hidden = self.forward(padded_starts, hidden)
            if gumbel_max_trick:
                gumbel = gumbel_sample(out.size()).to(device)
                out += gumbel
            out = out.argmax(dim=2)
            # withdraw last column
            last = out[:, -1].view(-1, 1)
            last_oh = torch.zeros(last.size()[0], self.vocabulary_size).to(device)
            last_oh = last_oh.scatter_(1, last, 1)
            starts = torch.cat((starts, last), dim=1)
            padded_starts = nn.utils.rnn.pad_sequence(starts, padding_value=self.padding_idx,
                                                      batch_first=True)
            it += 1

        # find indices of first stop token
        tmp = (padded_starts == self.stop_idx).float()
        idx = torch.arange(tmp.shape[1], 0, -1).float().to(device)
        tmp2 = tmp * idx
        indices = torch.argmax(tmp2, 1)
        indices = torch.where(indices != 0, indices,
                              starts.size()[1] * torch.ones(indices.size()).long().to(device))
        # withdraw elements after the first stop token
        indices_mask = torch.arange(padded_starts.size()[1]) * torch.ones(padded_starts.size())
        indices_mask = indices_mask.to(device)
        mask = indices_mask.long() <= indices.long().view(-1, 1).long()
        padded_starts = torch.where(mask, padded_starts,
                                    self.padding_idx * torch.ones(mask.size()).long().to(device))

        return padded_starts
