import numpy as np
import torch
import torch.nn.functional as F
from torch import nn

from utils import plot_learning_history


class RecurrentAE(nn.Module):
    def __init__(self, input_dim, hidden_dim, encoding_dim, alpha, padding_idx, stop_idx):
        """
        builds symmetrical autoencoder
        :input_dim: int
        :hidden_dims: iterable of ints
        """
        super().__init__()
        self.padding_idx = padding_idx
        self.stop_idx = stop_idx
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        # dimensions
        self.hidden_dim = hidden_dim
        self.encoding_dim = encoding_dim
        # encoding layers
        self.encoding_lstm = nn.LSTM(
            input_dim, hidden_dim, num_layers=1, batch_first=True
        )
        self.encoding_fc = nn.Linear(hidden_dim, encoding_dim)
        # decoding layers
        self.dec_lstm = nn.LSTM(encoding_dim, hidden_dim, num_layers=1)
        self.out_values_layer = nn.Linear(hidden_dim, input_dim)
        self.out_missing_layer = nn.Linear(hidden_dim, input_dim)
        # only for categorical
        # self.logsoftmax = nn.LogSoftmax(dim=1)
        self.binary_layer = nn.Sigmoid()
        # for loss
        self.bce_loss = nn.BCELoss(reduction='none')
        # self.mse_loss = nn.MSELoss(reduction='sum')
        self.alpha = alpha
        # for saving and loading purposes
        args = locals()
        self.args = {k: v for k, v in args.items() if k not in [
            'self', '__class__']}

    def forward(self, x, hidden_enc, hidden_dec):
        h = self.encode(x, hidden_enc)
        x_values, x_missings = self.decode(h, hidden_dec)
        return x_values, x_missings

    def init_hidden(self, size):
        weight = next(self.parameters()).data
        hidden = (weight.new(1, size, self.hidden_dim).zero_().to(self.device),
                  weight.new(1, size, self.hidden_dim).zero_().to(self.device))
        return hidden

    def encode(self, x, hidden):
        x, hidden = self.encoding_lstm(x, hidden)
        x = self.encoding_fc(x)
        x = F.sigmoid(x)
        return x

    def decode(self, x, hidden):
        x, hidden = self.dec_lstm(x, hidden)
        x_values = self.out_values_layer(x)
        x_missings = self.binary_layer(self.out_missing_layer(x))
        return x_values, x_missings

    def loss(
            self, x_values_tilde, x_missings_tilde,
            x_imputed, missing_values, missing_col_values):
        x_imputed = x_imputed.contiguous()
        x_values_tilde = x_values_tilde.contiguous()
        x_missings_tilde = x_missings_tilde.contiguous()
        missing_values = missing_values.contiguous()
        missing_col_values = missing_col_values.contiguous()
        # different necessary masks
        not_missing_mask = 1 - missing_values
        n_not_missing = not_missing_mask.sum()
        not_missing_col_mask = 1 - missing_col_values
        loss_mask = (not_missing_mask * not_missing_col_mask)
        n_values = loss_mask.sum()
        # missingness loss : we leave out from the loss padded item
        m_loss = (
            self.bce_loss(x_missings_tilde, not_missing_mask) *
            not_missing_col_mask
        ).sum() / n_not_missing

        # mse loss for values
        v_loss = ((x_values_tilde - x_imputed) **
                  2 * loss_mask).sum() / n_values

        return (1 - self.alpha) * v_loss + self.alpha * m_loss

    def train_model(
            self, train_loader, val_loader, optimizer, n_epochs,
            print_every_n_epoch=1, display=False):
        """
        :binary: bool, indicates that the targets will be binary and that the autoencoder has a sigmoid as output.
        """

        train_loss_history = np.zeros(n_epochs)
        val_loss_history = np.zeros(n_epochs)
        self.to(self.device)

        for epoch in range(n_epochs):
            # training
            self.train()
            train_loss = 0

            for it, (x_imputed, x_missings, x_col_missings) in enumerate(train_loader):
                optimizer.zero_grad()
                self.zero_grad()
                hidden_enc = self.init_hidden(x_imputed.size()[0])
                hidden_dec = self.init_hidden(x_imputed.size()[1])
                x_imputed = x_imputed.to(self.device)
                x_missings = x_missings.to(self.device)
                x_col_missings = x_col_missings.to(self.device)
                optimizer.zero_grad()
                x_values_tilde, x_missings_tilde = self.forward(
                    x_imputed, hidden_enc, hidden_dec)
                loss = self.loss(
                    x_values_tilde, x_missings_tilde,
                    x_imputed, x_missings, x_col_missings
                )
                loss.backward()
                optimizer.step()
                train_loss += loss.detach().cpu().numpy()

            n_train_batches = it
            train_loss_history[epoch] = train_loss / n_train_batches
            # evaluation
            val_loss = 0
            self.eval()
            for it, (x_imputed, x_missings, x_col_missings) in enumerate(val_loader):
                hidden_enc = self.init_hidden(x_imputed.size()[0])
                hidden_dec = self.init_hidden(x_imputed.size()[1])
                x_imputed = x_imputed.to(self.device)
                x_missings = x_missings.to(self.device)
                x_col_missings = x_col_missings.to(self.device)

                x_values_tilde, x_missings_tilde = self.forward(
                    x_imputed, hidden_enc, hidden_dec
                )

                loss = self.loss(
                    x_values_tilde, x_missings_tilde,
                    x_imputed, x_missings, x_col_missings
                ).cpu().detach().numpy()
                val_loss += loss

            n_val_batches = it
            val_loss_history[epoch] = val_loss / n_val_batches
            if (epoch + 1) % print_every_n_epoch == 0:
                print(
                    f'Epoch {epoch} training loss {train_loss / n_train_batches} validation loss {val_loss / it}')

        if display:
            plot_learning_history(train_loss_history, val_loss_history)
        self.eval()
        return train_loss_history, val_loss_history


    def evaluate(self, val_loader):

        self.eval()
        missing_accuracy_list = []
        regression_mse_list = []
        for (x_imputed, x_missings, x_col_missings) in val_loader:
            hidden_enc = self.init_hidden(x_imputed.size()[0])
            hidden_dec = self.init_hidden(x_imputed.size()[1])
            x_imputed = x_imputed.to(self.device)
            x_missings = x_missings.to(self.device)
            x_col_missings = x_col_missings.to(self.device)

            x_values_tilde, x_missings_tilde = self.forward(
                x_imputed, hidden_enc, hidden_dec)
            # missing binary accuracy
            missing_predictions = (x_missings_tilde >= 0.5).float()
            missing_golds = (x_missings + x_col_missings)
            n_predictions = x_missings_tilde.size(
            )[0] * x_missings_tilde.size()[1] * x_missings_tilde.size()[2]
            missing_acc = (missing_predictions ==
                           missing_golds).sum() / n_predictions
            missing_accuracy_list.append(missing_acc.detach().numpy())
            # regression mean squared error
            regression_mse = ((x_values_tilde - x_imputed)**2 *
                              (1 - missing_golds)).sum() / (1 - missing_golds).sum()
            regression_mse_list.append(regression_mse.detach().numpy())

        return np.mean(missing_accuracy_list), np.sqrt(np.mean(regression_mse_list))
